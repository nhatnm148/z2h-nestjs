export interface IDragonSlayerSchema {
    states: {
        idle: {},
        hero_fights_dragon_checking: {
            states: {
                hero_fetching: {
                    states: {
                        fetching: {},
                        done: {}
                    }
                },
                dragon_fetching: {
                    states: {
                        fetching: {},
                        done: {}
                    }
                }
            }
        },
        hero_fights_dragon_processing: {
            states: {
                fight_processing: {}
                hero_wins_processing: {
                    states: {
                        update_hero_processing: {
                            states: {
                                hero_updating: {},
                                update_hero_processing_done: {}
                            }
                        },
                        dragon_dies_processing: {
                            states: {
                                dragon_autopsying: {},
                                dragon_dies_processing_done: {}
                            }
                        },
                    }
                },
                hero_loses_processing: {
                    states: {
                        hero_dies_processing: {}
                    }
                },
                hero_fights_dragon_processing_done: {}
            }
        },
        done: {},
        errors: {}
    }
}

export enum DragonSlayerStates {
    IDLE = 'idle',
    HERO_FIGHTS_DRAGON_CHECKING = 'hero_fights_dragon_checking',
    HERO_FETCHING = 'hero_fetching',
    DRAGON_FETCHING = 'dragon_fetching',
    HERO_FIGHTS_DRAGON_PROCESSING = 'hero_fights_dragon_processing',
    FIGHT_PROCESSING = 'fight_processing',
    HERO_WINS_PROCESSING = 'hero_wins_processing',
    UPDATE_HERO_PROCESSING = 'update_hero_processing',
    HERO_UPDATING = 'hero_updating',
    UPDATE_HERO_PROCESSING_DONE = 'update_hero_processing_done',
    DRAGON_DIES_PROCESSING = 'dragon_dies_processing',
    DRAGON_AUTOPSYING = 'dragon_autopsying',
    DRAGON_DIES_PROCESSING_DONE = 'dragon_dies_processing_done',
    HERO_LOSES_PROCESSING = 'hero_loses_processing',
    HERO_DIES_PROCESSING = 'hero_dies_processing',
    HERO_DIES_PROCESSING_DONE = 'hero_dies_processing_done',
    HERO_FIGHTS_DRAGON_PROCESSING_DONE = 'hero_fights_dragon_processing_done',
    DONE = 'done',
    ERRORS = 'errors',
    FETCHING = 'fetching'
}
