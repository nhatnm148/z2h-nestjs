import { Injectable, Scope, Inject } from '@nestjs/common';
import { StateMachine, Interpreter, Machine, interpret, MachineOptions, assign, AnyEventObject } from 'xstate';
import {
    IDragonSlayerContext,
    IDragonSlayerSchema,
    dragonSlayerConfig,
    HeroFightsDragonContext
} from 'src/dragon-slayer-process-manager/@xstate/dragon-slayer.config';
import { DragonSlayerEvents, FetchHeroResponded, FetchDragonResponded, HeroWinsEvent, UpdateHeroResponded, DragonDiesResponded, HeroDiesResponded, HeroLosesEvent } from 'src/dragon-slayer-process-manager/@xstate/dragon-slayer.events';
import { RabbitMQService } from 'src/@msg-brokers/rabbitmq/rabbit-mq.service';
import { DragonSlayerStateCommands } from 'src/dragon-slayer-process-manager/@xstate/dragon-slayer-state-commands.enum';
import { RabbitMQQueues } from 'src/@msg-brokers/rabbitmq/rabbit-mq.enum';
import { FetchHeroRequest } from 'src/@msg-brokers/rabbitmq/models/dragon-slayer/fetch-hero-request.model';
import { FetchDragonRequest } from 'src/@msg-brokers/rabbitmq/models/dragon-slayer/fetch-dragon-request.model';
import { FightResult } from 'src/shared/models/fight-result.model';
import { DragonRefDocument } from 'src/shared/models/dragon-ref.document';
import { of, from, Observable } from 'rxjs';
import { HeroRefDocument } from 'src/shared/models/hero-ref.document';
import { UpdateHeroRequest } from 'src/@msg-brokers/rabbitmq/models/dragon-slayer/update-hero-request.model';
import { FetchDragonRequested } from 'src/@msg-brokers/rabbitmq/models/dragon-slayer/fetch-dragon-requested.model';
import { FetchHeroRequested } from 'src/@msg-brokers/rabbitmq/models/dragon-slayer/fetch-hero-requested.model';
import { DragonDiesRequest } from 'src/@msg-brokers/rabbitmq/models/dragon-slayer/dragon-dies-request.model';
import { UpdateHeroRequested } from 'src/@msg-brokers/rabbitmq/models/dragon-slayer/update-hero-requested.model';
import { DragonDiesRequested } from 'src/@msg-brokers/rabbitmq/models/dragon-slayer/dragon-dies-requested.model';
import { DragonSlayerRepositoryBase } from 'src/dragon-slayer-process-manager/repositories/dragon-slayer-repository.base';
import { DragonSlayerEventTypes } from 'src/dragon-slayer-process-manager/repositories/dragon-slayer-event.schema';
import { CommandStatuses } from 'src/shared/enumrations/command-statuses.enum';
import { HeroDiesRequest } from 'src/@msg-brokers/rabbitmq/models/dragon-slayer/hero-dies-request.model';
import { HeroDiesRequested } from 'src/@msg-brokers/rabbitmq/models/dragon-slayer/hero-dies-requested.model';

@Injectable({ scope: Scope.REQUEST })
export class DragonSlayerState {
    constructor(
        @Inject(DragonSlayerRepositoryBase.name)
        private dragonSlayerRepository: DragonSlayerRepositoryBase,
        private rabbitMQService: RabbitMQService
    ) { }

    private machine: StateMachine<
        IDragonSlayerContext,
        IDragonSlayerSchema,
        any>;
    private service: Interpreter<any>;

    private machineOptions: Partial<MachineOptions<
        IDragonSlayerContext, DragonSlayerEvents
    >> = {
            actions: {
                fetchHeroRespondedHandler: assign<any, DragonSlayerEvents>(
                    (_, event: FetchHeroResponded): Partial<IDragonSlayerContext> => {
                        return this.fetchHeroRespondedHandler(event);
                    }
                ),
                fetchDragonRespondedHandler: assign<any, DragonSlayerEvents>(
                    (_, event: FetchDragonResponded): Partial<IDragonSlayerContext> => {
                        return this.fetchDragonRespondedHandler(event);
                    }
                ),
                fetchHeroRequest: (context: HeroFightsDragonContext) => {
                    this.fetchHeroRequestHandler(context);
                },
                fetchDragonRequest: (context: HeroFightsDragonContext) => {
                    this.fetchDragonRequestHandler(context);
                },
                heroFightsDragonCheckingSucceedHandler: (context: HeroFightsDragonContext) => {
                    const { commandId, hero, dragon } = context;

                    this.dragonSlayerRepository.createEvent(
                        commandId,
                        `${DragonSlayerEventTypes.HERO_FIGHTS_DRAGON}_checking`,
                        CommandStatuses.SUCCEED,
                        { hero, dragon }
                    );
                },
                heroFightsDragonCheckingFailedHandler: (context: HeroFightsDragonContext) => {
                    const { commandId, heroId, dragonId } = context;

                    this.dragonSlayerRepository.createEvent(
                        commandId,
                        `${DragonSlayerEventTypes.HERO_FIGHTS_DRAGON}_checking`,
                        CommandStatuses.FAILED,
                        {
                            heroId,
                            dragonId
                        }
                    );
                },
                updateHeroRequest: (context: HeroFightsDragonContext, event: HeroWinsEvent) => {
                    this.updateHeroRequestHandler(context, event);
                },
                updateHeroRespondedHandler: assign<any, DragonSlayerEvents>(
                    (_, event: UpdateHeroResponded): Partial<HeroFightsDragonContext> => {
                        const { updatedHero, goldsGot } = event.updateHeroResponse;

                        return {
                            hero: updatedHero,
                            goldsGot
                        };
                    }
                ),
                dragonDiesRequest: (context: HeroFightsDragonContext) => {
                    this.dragonDiesRequestHandler(context);
                },
                dragonDiesRespondedHandler: assign<any, DragonSlayerEvents>(
                    (_, event: DragonDiesResponded): Partial<HeroFightsDragonContext> => {
                        const { updatedDragon } = event.dragonDiesResponse;

                        return {
                            dragon: updatedDragon
                        };
                    }
                ),
                heroDiesRequest: (context: HeroFightsDragonContext) => {
                    this.heroDiesRequestHandler(context);
                },
                heroDiesRespondedHandler: assign<any, DragonSlayerEvents>(
                    (_, event: HeroDiesResponded): Partial<HeroFightsDragonContext> => {
                        const { updatedHero } = event.heroDiesResponse;

                        return {
                            hero: updatedHero
                        };
                    }
                )
            },
            services: {
                processFight: (context: HeroFightsDragonContext) => {
                    const { hero, dragon } = context;

                    const result = this.calculateFight(hero, dragon);

                    const { isHeroWins } = result;

                    if (isHeroWins) {
                        return of(new HeroWinsEvent(result));
                    }

                    return of(new HeroLosesEvent());
                }
            },
            guards: {
                isAppropriateCommand: (context): boolean => {
                    return context.isAppropriateCommand === true;
                },
                isInappropriateCommand: (context): boolean => {
                    return context.isAppropriateCommand === false;
                },
                isHeroFightsDragonCommand: (context): boolean => {
                    return context.command === DragonSlayerStateCommands.HERO_FIGHTS_DRAGON;
                }
            }
        }

    initScripts(context: IDragonSlayerContext): Observable<any> {
        this.machine = Machine<
            IDragonSlayerContext,
            IDragonSlayerSchema
        >(dragonSlayerConfig)
            .withContext(context)
            .withConfig(this.machineOptions);

        this.service = interpret(this.machine)
            .onTransition(state => console.log(state.value))
            .start();

        return from(this.service);
    }

    transition(event: AnyEventObject): void {
        this.service.send(event);
    }

    stop(): void {
        this.service.stop();
    }

    getInappropriateCommandCase(): Partial<IDragonSlayerContext> {
        return {
            isAppropriateCommand: false
        };
    }

    fetchHeroRespondedHandler(event: FetchHeroResponded): Partial<HeroFightsDragonContext> {
        const { fetchedHero } = event;

        if (fetchedHero) {
            return { hero: fetchedHero };
        } else {
            return this.getInappropriateCommandCase();
        }
    }

    fetchDragonRespondedHandler(event: FetchDragonResponded): Partial<HeroFightsDragonContext> {
        const { fetchedDragon } = event;

        if (fetchedDragon) {
            return { dragon: fetchedDragon };
        } else {
            return this.getInappropriateCommandCase();
        }
    }

    async fetchHeroRequestHandler(context: HeroFightsDragonContext): Promise<void> {
        const { commandId: stateCommandId, heroId } = context;
        const rmqChannel = this.rabbitMQService.rabbitMQChannelInstance;
        const subQueueName = `${RabbitMQQueues.FETCH_HERO_REQUESTED_QUEUE}.${stateCommandId}`;

        await this.rabbitMQService.assertQueue(subQueueName);

        console.log('send', stateCommandId)
        this.rabbitMQService.send(
            RabbitMQQueues.FETCH_HERO_REQUEST_QUEUE,
            { commandId: stateCommandId, heroId } as FetchHeroRequest
        );

        rmqChannel.consume(
            subQueueName,
            data => {
                const parsedData: FetchHeroRequested =
                    this.rabbitMQService.parseData(data.content);

                const { commandId: resCommandId, fetchedHero } = parsedData;

                console.log('consume hero', resCommandId);
                console.log('consume hero', stateCommandId);

                if (resCommandId === stateCommandId) {
                    this.rabbitMQService.ack(data);
                    this.rabbitMQService.cancel(subQueueName);
                    this.rabbitMQService.deleteQueue(subQueueName);

                    this.transition(new FetchHeroResponded(fetchedHero));
                }
            },
            {
                consumerTag: subQueueName
            }
        );
    }

    async fetchDragonRequestHandler(context: HeroFightsDragonContext): Promise<void> {
        const { commandId: stateCommandId, dragonId } = context;
        const rmqChannel = this.rabbitMQService.rabbitMQChannelInstance;
        const subQueueName = `${RabbitMQQueues.FETCH_DRAGON_REQUESTED_QUEUE}.${stateCommandId}`;

        await this.rabbitMQService.assertQueue(subQueueName);

        this.rabbitMQService.send(
            RabbitMQQueues.FETCH_DRAGON_REQUEST_QUEUE,
            { commandId: stateCommandId, dragonId } as FetchDragonRequest
        );

        rmqChannel.consume(
            subQueueName,
            data => {
                const parsedData: FetchDragonRequested =
                    this.rabbitMQService.parseData(data.content);
                const { commandId: resCommandId, fetchedDragon } = parsedData;

                console.log('consume dragon', resCommandId);
                console.log('consume dragon', stateCommandId);

                if (resCommandId === stateCommandId) {
                    this.rabbitMQService.ack(data);
                    this.rabbitMQService.cancel(subQueueName);
                    this.rabbitMQService.deleteQueue(`${subQueueName}`);

                    this.transition(new FetchDragonResponded(fetchedDragon));
                }
            },
            {
                consumerTag: subQueueName
            }
        );
    }

    async updateHeroRequestHandler(
        context: HeroFightsDragonContext,
        event: HeroWinsEvent
    ): Promise<void> {
        const { commandId: stateCommandId, hero } = context;
        const { fightResult } = event;
        const rmqChannel = this.rabbitMQService.rabbitMQChannelInstance;
        const subQueueName = `${RabbitMQQueues.UPDATE_HERO_REQUESTED_QUEUE}.${stateCommandId}`;

        await this.rabbitMQService.assertQueue(subQueueName);

        this.rabbitMQService.send(
            RabbitMQQueues.UPDATE_HERO_REQUEST_QUEUE,
            { commandId: stateCommandId, hero, fightResult } as UpdateHeroRequest
        );

        rmqChannel.consume(
            subQueueName,
            data => {
                const parsedData: UpdateHeroRequested =
                    this.rabbitMQService.parseData(data.content);

                const { commandId: resCommandId } = parsedData; 

                if (stateCommandId === resCommandId) {
                    this.rabbitMQService.ack(data);
                    this.rabbitMQService.cancel(subQueueName);
                    this.rabbitMQService.deleteQueue(subQueueName);

                    this.transition(new UpdateHeroResponded(parsedData));
                }
            },
            {
                consumerTag: subQueueName
            }
        )
    }

    async dragonDiesRequestHandler(context: HeroFightsDragonContext): Promise<void> {
        const { commandId: stateCommandId, dragonId } = context;
        const rmqChannel = this.rabbitMQService.rabbitMQChannelInstance;
        const subQueueName = `${RabbitMQQueues.DRAGON_DIES_REQUESTED_QUEUE}.${stateCommandId}`;

        await this.rabbitMQService.assertQueue(subQueueName);

        this.rabbitMQService.send(
            RabbitMQQueues.DRAGON_DIES_REQUEST_QUEUE,
            { commandId: stateCommandId, dragonId } as DragonDiesRequest
        );

        rmqChannel.consume(
            subQueueName,
            data => {
                const parsedData: DragonDiesRequested =
                    this.rabbitMQService.parseData(data.content);

                const { commandId: resCommandId } = parsedData; 

                if (stateCommandId === resCommandId) {
                    this.rabbitMQService.ack(data);
                    this.rabbitMQService.cancel(subQueueName);
                    this.rabbitMQService.deleteQueue(subQueueName);

                    this.transition(new DragonDiesResponded(parsedData));
                }
            },
            {
                consumerTag: subQueueName
            }
        )
    }

    async heroDiesRequestHandler(context: HeroFightsDragonContext): Promise<void> {
        const { commandId: stateCommandId, heroId } = context;
        const rmqChannel = this.rabbitMQService.rabbitMQChannelInstance;
        const subQueueName = `${RabbitMQQueues.HERO_DIES_REQUESTED_QUEUE}.${stateCommandId}`;

        await this.rabbitMQService.assertQueue(subQueueName);

        this.rabbitMQService.send(
            RabbitMQQueues.HERO_DIES_REQUEST_QUEUE,
            { commandId: stateCommandId, heroId } as HeroDiesRequest
        );

        rmqChannel.consume(
            subQueueName,
            data => {
                const parsedData: HeroDiesRequested =
                    this.rabbitMQService.parseData(data.content);

                const { commandId: resCommandId } = parsedData; 

                if (stateCommandId === resCommandId) {
                    this.rabbitMQService.ack(data);
                    this.rabbitMQService.cancel(subQueueName);
                    this.rabbitMQService.deleteQueue(subQueueName);

                    this.transition(new HeroDiesResponded(parsedData));
                }
            },
            {
                consumerTag: subQueueName
            }
        )
    }

    // Returns TRUE if the hero wins
    calculateFight(hero: HeroRefDocument, dragon: DragonRefDocument): FightResult {
        const { stats: heroStats } = hero;
        const { stats: dragonStats } = dragon;

        const heroPower = this.calculatePower(heroStats.currentHp, heroStats.strength, true);
        const dragonPower = this.calculatePower(dragonStats.currentHp, dragonStats.strength, false);

        // const powerDiff = Math.abs(heroPower - dragonPower);
        let winningChance = 1 + ((heroPower / dragonPower) * 49);
        winningChance = winningChance > 100 ? 100 : winningChance;

        const isHeroWins = Math.floor((1 + Math.random() * 101)) <= winningChance;
        const hpLost = (dragonStats.strength * 3 * ((100 - winningChance) / 100));
        const isHeroDead = heroStats.currentHp <= hpLost;

        return { isHeroWins, isHeroDead, hpLost };
    }

    calculatePower(currentHp: number, strength: number, isHero: boolean): number {
        return isHero
            ? (currentHp + strength * 2) * 1.4
            : (currentHp + strength * 3);
    }
}
