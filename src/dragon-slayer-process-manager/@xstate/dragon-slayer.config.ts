import { MachineConfig } from 'xstate';
import { DragonSlayerStateCommands } from 'src/dragon-slayer-process-manager/@xstate/dragon-slayer-state-commands.enum';
import { FetchDragonResponded, FetchHeroResponded, HeroWinsEvent, UpdateHeroResponded, DragonDiesResponded, HeroLosesEvent, HeroDiesResponded } from 'src/dragon-slayer-process-manager/@xstate/dragon-slayer.events';
import { HeroRefDocument } from 'src/shared/models/hero-ref.document';
import { DragonRefDocument } from 'src/shared/models/dragon-ref.document';
import { CommandStatuses } from 'src/shared/enumrations/command-statuses.enum';
import { DragonSlayerStates } from 'src/dragon-slayer-process-manager/@xstate/dragon-slayer-states.enum';

export interface IDragonSlayerContext {
    commandId: string;
    command: DragonSlayerStateCommands,
    isAppropriateCommand: boolean,
    msg: string[],
    commandStatus: CommandStatuses;
}

export class HeroFightsDragonContext implements IDragonSlayerContext {
    commandId: string;
    command: DragonSlayerStateCommands;
    isAppropriateCommand: boolean;
    heroId: string;
    hero: HeroRefDocument;
    dragonId: string;
    dragon: DragonRefDocument;
    msg: string[];
    commandStatus: CommandStatuses;
    goldsGot: number;

    constructor(commandId: string, heroId: string, dragonId: string) {
        this.commandId = commandId;
        this.command = DragonSlayerStateCommands.HERO_FIGHTS_DRAGON;
        this.isAppropriateCommand = true;
        this.heroId = heroId || '';
        this.hero = null;
        this.dragonId = dragonId || '';
        this.dragon = null;
        this.msg = [];
        this.commandStatus = null;
        this.goldsGot = 0;
    }
}

export interface IDragonSlayerSchema {
    states: {
        idle: {},
        hero_fights_dragon_checking: {
            states: {
                hero_fetching: {
                    states: {
                        fetching: {},
                        done: {}
                    }
                },
                dragon_fetching: {
                    states: {
                        fetching: {},
                        done: {}
                    }
                }
            }
        },
        hero_fights_dragon_processing: {
            states: {
                fight_processing: {}
                hero_wins_processing: {
                    states: {
                        update_hero_processing: {
                            states: {
                                hero_updating: {},
                                update_hero_processing_done: {}
                            }
                        },
                        dragon_dies_processing: {
                            states: {
                                dragon_autopsying: {},
                                dragon_dies_processing_done: {}
                            }
                        },
                    }
                },
                hero_loses_processing: {
                    states: {
                        hero_dies_processing: {},
                        hero_dies_processing_done: {}
                    }
                },
                hero_fights_dragon_processing_done: {}
            }
        },
        done: {},
        errors: {}
    }
}

export const dragonSlayerConfig: MachineConfig<
    IDragonSlayerContext,
    IDragonSlayerSchema,
    any
> = {
    id: 'dragon_slayer_scripts',
    initial: DragonSlayerStates.IDLE,
    states: {
        idle: {
            always: [
                {
                    target: DragonSlayerStates.HERO_FIGHTS_DRAGON_CHECKING,
                    cond: 'isHeroFightsDragonCommand'
                }
            ]
        },
        [DragonSlayerStates.HERO_FIGHTS_DRAGON_CHECKING]: {
            type: 'parallel',
            states: {
                [DragonSlayerStates.HERO_FETCHING]: {
                    initial: DragonSlayerStates.FETCHING,
                    states: {
                        [DragonSlayerStates.FETCHING]: {
                            entry: 'fetchHeroRequest',
                            on: {
                                [FetchHeroResponded.type]: {
                                    target: DragonSlayerStates.DONE,
                                    actions: 'fetchHeroRespondedHandler'
                                }
                            }
                        },
                        [DragonSlayerStates.DONE]: {
                            type: 'final'
                        }
                    }
                },
                [DragonSlayerStates.DRAGON_FETCHING]: {
                    initial: DragonSlayerStates.FETCHING,
                    states: {
                        [DragonSlayerStates.FETCHING]: {
                            entry: 'fetchDragonRequest',
                            on: {
                                [FetchDragonResponded.type]: {
                                    target: DragonSlayerStates.DONE,
                                    actions: 'fetchDragonRespondedHandler'
                                }
                            }
                        },
                        [DragonSlayerStates.DONE]: {
                            type: 'final'
                        }
                    }
                }
            },
            onDone: [
                {
                    target: DragonSlayerStates.HERO_FIGHTS_DRAGON_PROCESSING,
                    cond: 'isAppropriateCommand',
                    actions: 'heroFightsDragonCheckingSucceedHandler'
                },
                {
                    target: DragonSlayerStates.ERRORS,
                    cond: 'isInappropriateCommand',
                    actions: 'heroFightsDragonCheckingFailedHandler'
                }
            ]
        },
        [DragonSlayerStates.HERO_FIGHTS_DRAGON_PROCESSING]: {
            initial: DragonSlayerStates.FIGHT_PROCESSING,
            states: {
                [DragonSlayerStates.FIGHT_PROCESSING]: {
                    invoke: {
                        id: 'processFight',
                        src: 'processFight'
                    },
                    on: {
                        [HeroWinsEvent.type]: DragonSlayerStates.HERO_WINS_PROCESSING,
                        [HeroLosesEvent.type]: DragonSlayerStates.HERO_LOSES_PROCESSING
                    }
                },
                [DragonSlayerStates.HERO_WINS_PROCESSING]: {
                    type: 'parallel',
                    states: {
                        [DragonSlayerStates.UPDATE_HERO_PROCESSING]: {
                            initial: DragonSlayerStates.HERO_UPDATING,
                            states: {
                                [DragonSlayerStates.HERO_UPDATING]: {
                                    entry: 'updateHeroRequest',
                                    on: {
                                        [UpdateHeroResponded.type]: {
                                            target: DragonSlayerStates.UPDATE_HERO_PROCESSING_DONE,
                                            actions: 'updateHeroRespondedHandler'
                                        }
                                    }
                                },
                                [DragonSlayerStates.UPDATE_HERO_PROCESSING_DONE]: {
                                    type: 'final'
                                }
                            }
                        },
                        [DragonSlayerStates.DRAGON_DIES_PROCESSING]: {
                            initial: DragonSlayerStates.DRAGON_AUTOPSYING,
                            states: {
                                [DragonSlayerStates.DRAGON_AUTOPSYING]: {
                                    entry: 'dragonDiesRequest',
                                    on: {
                                        [DragonDiesResponded.type]: {
                                            target: DragonSlayerStates.DRAGON_DIES_PROCESSING_DONE,
                                            actions: 'dragonDiesRespondedHandler'
                                        }
                                    }
                                },
                                [DragonSlayerStates.DRAGON_DIES_PROCESSING_DONE]: {
                                    type: 'final'
                                }
                            }
                        }
                    },
                    onDone: DragonSlayerStates.HERO_FIGHTS_DRAGON_PROCESSING_DONE
                },
                [DragonSlayerStates.HERO_LOSES_PROCESSING]: {
                    initial: DragonSlayerStates.HERO_DIES_PROCESSING,
                    states: {
                        [DragonSlayerStates.HERO_DIES_PROCESSING]: {
                            entry: 'heroDiesRequest',
                            on: {
                                [HeroDiesResponded.type]: {
                                    target: DragonSlayerStates.HERO_DIES_PROCESSING_DONE,
                                    actions: 'heroDiesRespondedHandler'
                                }
                            }
                        },
                        [DragonSlayerStates.HERO_DIES_PROCESSING_DONE]: {
                            type: 'final'
                        }
                    },
                    onDone: DragonSlayerStates.HERO_FIGHTS_DRAGON_PROCESSING_DONE
                },
                [DragonSlayerStates.HERO_FIGHTS_DRAGON_PROCESSING_DONE]: {
                    type: 'final'
                }
            },
            onDone: DragonSlayerStates.DONE
        },
        [DragonSlayerStates.DONE]: {
            type: 'final'
        },
        [DragonSlayerStates.ERRORS]: {
            type: 'final'
        }
    }
};
