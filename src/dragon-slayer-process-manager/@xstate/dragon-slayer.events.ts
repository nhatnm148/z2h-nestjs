import { EventObject } from 'xstate';
import { HeroRefDocument } from 'src/shared/models/hero-ref.document';
import { DragonRefDocument } from 'src/shared/models/dragon-ref.document';
import { FightResult } from 'src/shared/models/fight-result.model';
import { UpdateHeroRequested } from 'src/@msg-brokers/rabbitmq/models/dragon-slayer/update-hero-requested.model';
import { DragonDiesRequested } from 'src/@msg-brokers/rabbitmq/models/dragon-slayer/dragon-dies-requested.model';
import { HeroDiesRequested } from 'src/@msg-brokers/rabbitmq/models/dragon-slayer/hero-dies-requested.model';

export class FetchHeroResponded implements EventObject {
    static readonly type = '[DRAGON_SLAYER] Fetch Hero Responded';
    readonly type = FetchHeroResponded.type;

    constructor(
        public fetchedHero: HeroRefDocument
    ) { }
}

export class FetchDragonResponded implements EventObject {
    static readonly type = '[DRAGON_SLAYER] Fetch Dragon Responded';
    readonly type = FetchDragonResponded.type;

    constructor(
        public fetchedDragon: DragonRefDocument
    ) { }
}

export class HeroWinsEvent implements EventObject {
    static readonly type = '[DRAGON_SLAYER] Hero Wins Event';
    readonly type = HeroWinsEvent.type;

    constructor(
        public fightResult: FightResult
    ) { }
}

export class HeroLosesEvent implements EventObject {
    static readonly type = '[DRAGON_SLAYER] Hero Loses Event';
    readonly type = HeroLosesEvent.type;
}

export class UpdateHeroResponded implements EventObject {
    static readonly type = '[DRAGON_SLAYER] Update Hero Responded';
    readonly type = UpdateHeroResponded.type;

    constructor(
        public updateHeroResponse: UpdateHeroRequested
    ) { }
}

export class DragonDiesResponded implements EventObject {
    static readonly type = '[DRAGON_SLAYER] Dragon Dies Responded';
    readonly type = DragonDiesResponded.type;

    constructor(
        public dragonDiesResponse: DragonDiesRequested
    ) { }
}

export class HeroDiesResponded implements EventObject {
    static readonly type = '[DRAGON_SLAYER] Hero Dies Responded';
    readonly type = HeroDiesResponded.type;

    constructor(
        public heroDiesResponse: HeroDiesRequested
    ) { }
}

export type DragonSlayerEvents =
    FetchDragonResponded | FetchHeroResponded | HeroWinsEvent | UpdateHeroResponded |
    DragonDiesResponded | HeroLosesEvent;
