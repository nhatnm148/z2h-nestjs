import { Document } from 'mongoose';

export interface HeroDocument extends Document {
    id: string;
    name: string;
    stats: Stats;
}

export class Stats {
    hp: number;
    currentHp: number;
    strength: number;
}
