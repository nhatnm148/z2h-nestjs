import { Module } from '@nestjs/common';
import { DragonSlayerProcessManagerController } from './dragon-slayer-process-manager.controller';
import { DragonSlayerCommandsHandlers } from 'src/dragon-slayer-process-manager/commands/handlers/dragon-slayer.handlers';
import { DragonSlayerState } from 'src/dragon-slayer-process-manager/@xstate/dragon-slayer.state';
import { SharedModule } from 'src/shared/shared.module';
import { DragonSlayerProcessManagerService } from './dragon-slayer-process-manager.service';
import { DragonSlayerEventsHandler } from 'src/dragon-slayer-process-manager/dragon-slayer-events.handler';
import {
  DragonSlayerEventCollectionProvider,
  DragonSlayerRepositoryProvider
} from 'src/dragon-slayer-process-manager/repositories/dragon-slayer.provider';
import { DragonSlayerProcessManagerGateway } from 'src/dragon-slayer-process-manager/dragon-slayer-process-manager.gateway';

@Module({
  imports: [
    SharedModule
  ],
  controllers: [
    DragonSlayerProcessManagerController
  ],
  providers: [
    ...DragonSlayerCommandsHandlers,
    DragonSlayerEventCollectionProvider,
    DragonSlayerRepositoryProvider,
    DragonSlayerState,
    DragonSlayerProcessManagerService,
    DragonSlayerProcessManagerGateway,
    DragonSlayerEventsHandler
  ]
})
export class DragonSlayerProcessManagerModule { }
