import { ICommand } from '@nestjs/cqrs';
import { DragonSlayerState } from 'src/dragon-slayer-process-manager/@xstate/dragon-slayer.state';

export class HeroFightsDragonCommand implements ICommand {
    constructor(
        public commandId: string,
        public heroId: string,
        public dragonId: string,
        public dragonSlayerState: DragonSlayerState
    ) { }
}
