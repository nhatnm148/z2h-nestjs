import { CommandHandler, ICommandHandler } from '@nestjs/cqrs';
import { HeroFightsDragonCommand } from 'src/dragon-slayer-process-manager/commands/iplm/hero-fights-dragon.command';
import { HeroFightsDragonContext } from 'src/dragon-slayer-process-manager/@xstate/dragon-slayer.config';
import { DragonSlayerRepositoryBase } from 'src/dragon-slayer-process-manager/repositories/dragon-slayer-repository.base';
import { DragonSlayerEventTypes } from 'src/dragon-slayer-process-manager/repositories/dragon-slayer-event.schema';
import { CommandStatuses } from 'src/shared/enumrations/command-statuses.enum';
import { Inject } from '@nestjs/common';
import { DragonSlayerProcessManagerGateway } from 'src/dragon-slayer-process-manager/dragon-slayer-process-manager.gateway';
import { DragonSlayerStates } from 'src/dragon-slayer-process-manager/@xstate/dragon-slayer-states.enum';

@CommandHandler(HeroFightsDragonCommand)
export class HeroFightsDragonHandler implements ICommandHandler<HeroFightsDragonCommand> {
    constructor(
        @Inject(DragonSlayerRepositoryBase.name)
        private dragonSlayerRepository: DragonSlayerRepositoryBase,
        private dragonSlayerProcessManagerGateway: DragonSlayerProcessManagerGateway
    ) { }

    async execute(command: HeroFightsDragonCommand) {
        const { commandId, heroId, dragonId, dragonSlayerState } = command;

        const context = new HeroFightsDragonContext(commandId, heroId, dragonId);

        const state$ = dragonSlayerState.initScripts(context);

        state$.subscribe(state => {
            if (state.done) {
                dragonSlayerState.stop();
                const context: Partial<HeroFightsDragonContext> = state.context;

                if (state.value === DragonSlayerStates.DONE) {
                    this.dragonSlayerProcessManagerGateway.send(context.commandId, context);

                    this.dragonSlayerRepository.createEvent(
                        context.commandId,
                        `${DragonSlayerEventTypes.HERO_FIGHTS_DRAGON}_final_result`,
                        CommandStatuses.SUCCEED,
                        {
                            afterFightHero: context.hero,
                            afterFightDragon: context.dragon,
                            goldsGot: context.goldsGot
                        }
                    );
                } else if (state.value === DragonSlayerStates.ERRORS) {
                    this.dragonSlayerProcessManagerGateway.send(context.commandId, context);
                }
            }
        });
    }
}
