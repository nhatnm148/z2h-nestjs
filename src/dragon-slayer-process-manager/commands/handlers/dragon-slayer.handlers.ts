import { HeroFightsDragonHandler } from 'src/dragon-slayer-process-manager/commands/handlers/hero-fights-dragon.handler';

export const DragonSlayerCommandsHandlers = [HeroFightsDragonHandler];