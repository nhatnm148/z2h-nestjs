import { RabbitMQService } from 'src/@msg-brokers/rabbitmq/rabbit-mq.service';
import { Injectable } from '@nestjs/common';
import { RabbitMQQueues } from 'src/@msg-brokers/rabbitmq/rabbit-mq.enum';
import { DragonSlayerProcessManagerService } from 'src/dragon-slayer-process-manager/dragon-slayer-process-manager.service';

@Injectable()
export class DragonSlayerEventsHandler {
    constructor(
        private rabbitMQService: RabbitMQService,
        private dragonSlayerProcessManagerService: DragonSlayerProcessManagerService
    ) {
        // this.registerToQueues$();
    }

    registerToQueues$(): void {
        const rmqChannel = this.rabbitMQService.rabbitMQChannelInstance;
    }
}
