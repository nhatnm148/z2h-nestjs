import { Provider } from '@nestjs/common';
import { MongoDBEnums } from 'src/@databases/mongodb/mongodb-collections.enum';
import { Connection } from 'mongoose';
import { DragonSlayerEventSchema } from 'src/dragon-slayer-process-manager/repositories/dragon-slayer-event.schema';
import { DragonSlayerRepositoryBase } from 'src/dragon-slayer-process-manager/repositories/dragon-slayer-repository.base';
import { DragonSlayerRepository } from 'src/dragon-slayer-process-manager/repositories/dragon-slayer.repository';

export const DragonSlayerEventCollectionProvider: Provider = {
    provide: MongoDBEnums.DRAGON_SLAYER_EVENT_CQRS_COLLECTION,
    useFactory: (connection: Connection) => {
        return connection.model(
            MongoDBEnums.DRAGON_SLAYER_EVENT_CQRS_COLLECTION,
            DragonSlayerEventSchema
        );
    },
    inject: [MongoDBEnums.GAME_CQRS_CONNECTION_TOKEN]
};

export const DragonSlayerRepositoryProvider: Provider = {
    provide: DragonSlayerRepositoryBase,
    useClass: DragonSlayerRepository,
    inject: [MongoDBEnums.DRAGON_SLAYER_EVENT_CQRS_COLLECTION]
}
