import { Schema } from 'mongoose';
import { v4 } from 'uuid';
import { CommandStatuses } from 'src/shared/enumrations/command-statuses.enum';

export enum DragonSlayerEventTypes {
    HERO_FIGHTS_DRAGON = 'hero-fights-dragon'
}

export const DragonSlayerEventSchema = new Schema({
    _id: { type: String, default: v4 },
    id: { type: String },
    type: { type: DragonSlayerEventTypes },
    date: { type: Date },
    status: { type: CommandStatuses },
    payload: { type: Object }
});
