import { DragonSlayerRepositoryBase } from 'src/dragon-slayer-process-manager/repositories/dragon-slayer-repository.base';
import { DragonSlayerEventDocument } from 'src/dragon-slayer-process-manager/repositories/dragon-slayer-event.document';
import { Inject } from '@nestjs/common';
import { MongoDBEnums } from 'src/@databases/mongodb/mongodb-collections.enum';
import { Model } from 'mongoose';
import { Observable, from } from 'rxjs';
import { CommandStatuses } from 'src/shared/enumrations/command-statuses.enum';

export class DragonSlayerRepository implements DragonSlayerRepositoryBase {
    constructor(
        @Inject(MongoDBEnums.DRAGON_SLAYER_EVENT_CQRS_COLLECTION)
        private dragonSlayerRepository: Model<DragonSlayerEventDocument>
    ) { }

    createEvent(
        id: string, type: string,
        status: CommandStatuses, payload: any
    ): Observable<any> {
        const createdPayload = {
            id,
            type,
            date: new Date().toISOString(),
            status,
            payload
        };

        return from(this.dragonSlayerRepository.insertMany(createdPayload));
    }
} 