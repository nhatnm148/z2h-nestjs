/* eslint-disable @typescript-eslint/no-unused-vars */
import { Observable } from 'rxjs';
import { DragonSlayerEventTypes } from 'src/dragon-slayer-process-manager/repositories/dragon-slayer-event.schema';
import { CommandStatuses } from 'src/shared/enumrations/command-statuses.enum';

export abstract class DragonSlayerRepositoryBase {
    createEvent(
        id: string,
        type: string,
        status: CommandStatuses,
        payload: any
    ): Observable<any> {
        return null;
    }
}
