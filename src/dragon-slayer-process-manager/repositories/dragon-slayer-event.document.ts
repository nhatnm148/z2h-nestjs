import { Document } from 'mongoose';
import { DragonSlayerEventTypes } from 'src/dragon-slayer-process-manager/repositories/dragon-slayer-event.schema';
import { CommandStatuses } from 'src/shared/enumrations/command-statuses.enum';

export class DragonSlayerEventDocument extends Document {
    id: string;
    type: DragonSlayerEventTypes;
    date: Date;
    status: CommandStatuses;
    payload: any;
}
