import { Controller, Post, Param } from '@nestjs/common';
import { DragonSlayerProcessManagerService } from 'src/dragon-slayer-process-manager/dragon-slayer-process-manager.service';

@Controller()
export class DragonSlayerProcessManagerController {
    constructor(
        private dragonSlayerProcessManagerService: DragonSlayerProcessManagerService
    ) { }

    @Post('/hero-fights-dragon-command/:heroId/:dragonId/:commandId')
    heroFightsDragon(
        @Param('heroId') heroId: string,
        @Param('dragonId') dragonId: string,
        @Param('commandId') commandId: string
    ) {
        this.dragonSlayerProcessManagerService
            .heroFightsDragon(commandId, heroId, dragonId);
    }
}
