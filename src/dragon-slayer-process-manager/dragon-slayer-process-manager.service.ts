import { Injectable, Scope } from '@nestjs/common';
import { CommandBus } from '@nestjs/cqrs';
import { HeroFightsDragonCommand } from 'src/dragon-slayer-process-manager/commands/iplm/hero-fights-dragon.command';
import { DragonSlayerState } from 'src/dragon-slayer-process-manager/@xstate/dragon-slayer.state';

@Injectable({ scope: Scope.REQUEST })
export class DragonSlayerProcessManagerService {
    constructor(
        private commandBus: CommandBus,
        private dragonSlayerState: DragonSlayerState
    ) { }

    async heroFightsDragon(
        commandId: string, heroId: string, dragonId: string
    ): Promise<any> {
        return await this.commandBus.execute(
            new HeroFightsDragonCommand(commandId, heroId, dragonId, this.dragonSlayerState)
        );
    }
}
