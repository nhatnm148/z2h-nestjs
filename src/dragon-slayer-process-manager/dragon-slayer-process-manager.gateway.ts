import {
    WebSocketGateway,
    WebSocketServer
} from '@nestjs/websockets';

@WebSocketGateway()
export class DragonSlayerProcessManagerGateway {
    @WebSocketServer() server;

    getServer(): any {
        return this.server;
    }

    send(event: string, msg: any): void {
        this.server.emit(event, msg);
    }
}
