import { Module, Global } from '@nestjs/common';
import { ConfigServiceProvider } from 'src/@config/config-manager/config-service.provider';
import { ConfigService } from 'src/@config/config-manager/config.service';

@Global()
@Module({
  controllers: [],
  providers: [
    ConfigServiceProvider
  ],
  exports: [
    ConfigService
  ]
})
export class ConfigModule {}
