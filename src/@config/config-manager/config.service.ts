import { Injectable } from '@nestjs/common';
import * as dotenv from 'dotenv';
import { readFileSync } from 'fs';

@Injectable()
export class ConfigService {
    private static instance: ConfigService = null;
    // eslint-disable-next-line @typescript-eslint/ban-types
    private static queues: Array<Object>;

    private constructor(filePath: string) {
        // const path = join(filePath);
        dotenv.config({ path: filePath });
    }
    
    static getInstanceAsync(envPath?: string): ConfigService {
        if (!this.instance) {
            const defaultEnvPath = `.env.${process.env.NODE_ENV}`;
            this.getEvents();
            this.instance = new ConfigService(`.env.${envPath}` || defaultEnvPath);
        }

        return this.instance;
    }
    
    static getEvents(): void {
        const eventsManager = JSON.parse(readFileSync('events-manager.json').toString());
        this.queues = eventsManager.queues.filter(queue => !!queue);
    }

    static getQueues() {
        return this.queues;
    }

    get(key: string): string {
        return process.env[key]; // process.env['M'] === process.env.M  
    }
}
