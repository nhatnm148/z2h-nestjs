import { Provider } from '@nestjs/common';
import { ConfigService } from 'src/@config/config-manager/config.service';

export const ConfigServiceProvider: Provider = {
    provide: ConfigService,
    useFactory: (): ConfigService => {
        return ConfigService.getInstanceAsync(process.env.NODE_ENV)
    }
}
