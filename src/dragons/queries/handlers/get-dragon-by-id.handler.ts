import { QueryHandler, IQueryHandler, EventBus } from '@nestjs/cqrs';
import { GetDragonByIdQuery } from 'src/dragons/queries/iplm/get-dragon-by-id.query';
import { DragonQueryDocument } from 'src/dragons/repository/documents/dragon-query.document';
import { Inject } from '@nestjs/common';
import { DragonRepositoryBase } from 'src/dragons/repository/dragon-repository.base';
import { FetchDragonRequested } from 'src/@msg-brokers/rabbitmq/models/dragon-slayer/fetch-dragon-requested.model';
import { DragonRefDocument } from 'src/shared/models/dragon-ref.document';
import { GotDragonByIdEvent } from 'src/dragons/events/iplm/got-dragon-by-id.event';

@QueryHandler(GetDragonByIdQuery)
export class GetDragonByIdHandler implements IQueryHandler<GetDragonByIdQuery> {
    constructor(
        @Inject(DragonRepositoryBase.name)
        private dragonRepositoryBase: DragonRepositoryBase,
        private eventBus: EventBus
    ) { }

    async execute(query: GetDragonByIdQuery): Promise<DragonQueryDocument> {
        const { dragonId, isInternal, commandId, queueName } = query;

        const fetchedDragon: DragonRefDocument = await this.dragonRepositoryBase
            .getDragonById(dragonId)
            .toPromise();

        if (!isInternal) {
            const subQueueName = `${queueName}.${commandId}`;
            const payload: FetchDragonRequested = {
                commandId,
                fetchedDragon
            };

            this.eventBus.publish(new GotDragonByIdEvent(subQueueName, payload));
        }

        return fetchedDragon;
    }
}
