import { GetDragonByIdHandler } from 'src/dragons/queries/handlers/get-dragon-by-id.handler';

export const DragonQueriesHandlers = [GetDragonByIdHandler];
