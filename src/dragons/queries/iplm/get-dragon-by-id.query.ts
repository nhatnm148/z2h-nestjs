import { IQuery } from '@nestjs/cqrs';

export class GetDragonByIdQuery implements IQuery {
    constructor(
        public isInternal: boolean,
        public dragonId: string,
        public commandId: string,
        public queueName?: string
    ) { }
}
