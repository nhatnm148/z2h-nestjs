import { Module } from '@nestjs/common';
import { DragonsService } from './dragons.service';
import { DragonsController } from './dragons.controller';
import { DragonCollectionsProviders, DragonRepositoryProvider } from 'src/dragons/repository/dragon.providers';
import { DragonsEventsHandler } from 'src/dragons/dragons-events.handler';
import { DragonCommandsHandlers } from 'src/dragons/commands/handlers/dragon-commands.handlers';
import { CqrsModule } from '@nestjs/cqrs';
import { DragonEventsHandlers } from 'src/dragons/events/handlers/dragon-events.handlers';
import { DragonQueriesHandlers } from 'src/dragons/queries/handlers/dragon-queries.handlers';

@Module({
  imports: [
    CqrsModule
  ],
  providers: [
    DragonsService,
    ...DragonCollectionsProviders,
    DragonRepositoryProvider,
    DragonsEventsHandler,
    ...DragonCommandsHandlers,
    ...DragonEventsHandlers,
    ...DragonQueriesHandlers
  ],
  controllers: [
    DragonsController
  ],
  exports: [
    DragonRepositoryProvider
  ]
})
export class DragonsModule {}
