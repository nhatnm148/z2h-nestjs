import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsNotEmptyObject } from 'class-validator';
import { Stats } from 'src/dragons/repository/documents/dragon-query.document';

export class CreateDragonDto {
    @ApiProperty()
    @IsNotEmpty()
    name: string;

    @ApiProperty()
    @IsNotEmptyObject()
    stats: Stats;
}
