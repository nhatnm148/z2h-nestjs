import { Injectable } from '@nestjs/common';
import { RabbitMQService } from 'src/@msg-brokers/rabbitmq/rabbit-mq.service';
import { RabbitMQQueues } from 'src/@msg-brokers/rabbitmq/rabbit-mq.enum';
import { DragonsService } from 'src/dragons/dragons.service';
import { FetchDragonRequest } from 'src/@msg-brokers/rabbitmq/models/dragon-slayer/fetch-dragon-request.model';
import { DragonDiesRequest } from 'src/@msg-brokers/rabbitmq/models/dragon-slayer/dragon-dies-request.model';

@Injectable()
export class DragonsEventsHandler {
    constructor(
        private rabbitMQService: RabbitMQService,
        private dragonsService: DragonsService
    ) {
        this.registerToQueues$();
    }

    registerToQueues$(): void {
        const rmqChannel = this.rabbitMQService.rabbitMQChannelInstance;

        // Fetch hero request listener
        rmqChannel.consume(
            RabbitMQQueues.FETCH_DRAGON_REQUEST_QUEUE,
            data => {
                this.fetchDragonRequestHandler(this.rabbitMQService.parseData(data.content));
                this.rabbitMQService.ack(data);
            }
        );

        rmqChannel.consume(
            RabbitMQQueues.DRAGON_DIES_REQUEST_QUEUE,
            data => {
                this.dragonDiesRequestHandler(this.rabbitMQService.parseData(data.content));
                this.rabbitMQService.ack(data);
            }
        );
    }

    fetchDragonRequestHandler(data: FetchDragonRequest): void {
        this.dragonsService.fetchDragonRequestHandler(data);
    }

    dragonDiesRequestHandler(data: DragonDiesRequest): void {
        this.dragonsService.dragonDiesRequestHandler(data);
    }
}
