import { Controller, Get, Post, Body, ValidationPipe } from '@nestjs/common';
import { Observable } from 'rxjs';
import { DragonQueryDocument } from 'src/dragons/repository/documents/dragon-query.document';
import { DragonsService } from 'src/dragons/dragons.service';
import { CreateDragonDto } from 'src/dragons/dtos/create-dragon.dto';

@Controller()
export class DragonsController {
    constructor(
        private dragonsService: DragonsService
    ) { }
    
    @Get()
    getDragons(): Observable<DragonQueryDocument[]> {
        return this.dragonsService.getDragons();
    }

    @Post()
    async createDragon(
        @Body(ValidationPipe) createDragonDto: CreateDragonDto
    ): Promise<DragonQueryDocument> {
        return await this.dragonsService.createDragon(createDragonDto);
    }
}
