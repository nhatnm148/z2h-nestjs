import { IEvent } from '@nestjs/cqrs';

export class DragonDiedRequestEvent implements IEvent {
    constructor(
        public dragonId: string,
        public commandId: string,
        public queueName: string
    ) { }
}
