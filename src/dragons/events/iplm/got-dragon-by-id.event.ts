import { IEvent } from '@nestjs/cqrs';
import { FetchDragonRequested } from 'src/@msg-brokers/rabbitmq/models/dragon-slayer/fetch-dragon-requested.model';

export class GotDragonByIdEvent implements IEvent {
    constructor(
        public queueName: string,
        public payload: FetchDragonRequested
    ) { }
}
