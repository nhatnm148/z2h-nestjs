import { GotDragonByIdHandler } from 'src/dragons/events/handlers/got-dragon-by-id.handler';
import { DragonDiedRequestHandler } from 'src/dragons/events/handlers/dragon-died-request.handler';

export const DragonEventsHandlers = [GotDragonByIdHandler, DragonDiedRequestHandler];
