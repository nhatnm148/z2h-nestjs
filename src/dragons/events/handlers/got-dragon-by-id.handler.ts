import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { GotDragonByIdEvent } from 'src/dragons/events/iplm/got-dragon-by-id.event';
import { RabbitMQService } from 'src/@msg-brokers/rabbitmq/rabbit-mq.service';

@EventsHandler(GotDragonByIdEvent)
export class GotDragonByIdHandler implements IEventHandler<GotDragonByIdEvent> {
    constructor(
        private rabbitMQService: RabbitMQService
    ) { }

    handle(event: GotDragonByIdEvent): void {
        const { queueName, payload } = event;

        this.rabbitMQService.send(queueName, payload);
    }
}