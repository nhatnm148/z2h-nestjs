import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { DragonDiedRequestEvent } from 'src/dragons/events/iplm/dragon-died-request.event';
import { DragonRepositoryBase } from 'src/dragons/repository/dragon-repository.base';
import { Inject } from '@nestjs/common';
import { RabbitMQService } from 'src/@msg-brokers/rabbitmq/rabbit-mq.service';
import { DragonDiesRequested } from 'src/@msg-brokers/rabbitmq/models/dragon-slayer/dragon-dies-requested.model';

@EventsHandler(DragonDiedRequestEvent)
export class DragonDiedRequestHandler implements IEventHandler<DragonDiedRequestEvent> {
    constructor(
        @Inject(DragonRepositoryBase.name)
        private dragonRepository: DragonRepositoryBase,
        private rabbitMQService: RabbitMQService
    ) { }

    async handle(event: DragonDiedRequestEvent): Promise<any> {
        const { dragonId, commandId, queueName } = event;

        const updatedDragon = await this.dragonRepository
            .goodbyeDragon(dragonId)
            .toPromise();

        const subQueueName = `${queueName}.${commandId}`;
        const payload: DragonDiesRequested = {
            commandId,
            updatedDragon
        };
        
        this.rabbitMQService.send(subQueueName, payload);
    }
}
