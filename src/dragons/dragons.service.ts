import { Injectable, Inject } from '@nestjs/common';
import { DragonRepositoryBase } from 'src/dragons/repository/dragon-repository.base';
import { Observable } from 'rxjs';
import { DragonQueryDocument } from 'src/dragons/repository/documents/dragon-query.document';
import { CreateDragonDto } from 'src/dragons/dtos/create-dragon.dto';
import { FetchDragonRequest } from 'src/@msg-brokers/rabbitmq/models/dragon-slayer/fetch-dragon-request.model';
import { RabbitMQService } from 'src/@msg-brokers/rabbitmq/rabbit-mq.service';
import { RabbitMQQueues } from 'src/@msg-brokers/rabbitmq/rabbit-mq.enum';
import { DragonDiesRequest } from 'src/@msg-brokers/rabbitmq/models/dragon-slayer/dragon-dies-request.model';
import { CommandBus, QueryBus } from '@nestjs/cqrs';
import { CreateDragonCommand } from 'src/dragons/commands/iplm/create-dragon.command';
import { GetDragonByIdQuery } from 'src/dragons/queries/iplm/get-dragon-by-id.query';
import { DragonDiesRequestCommand } from 'src/dragons/commands/iplm/dragon-dies-request.command';

@Injectable()
export class DragonsService {
    constructor(
        @Inject(DragonRepositoryBase.name)
        private dragonRepository: DragonRepositoryBase,
        private rabbitMQService: RabbitMQService,
        private commandBus: CommandBus,
        private queryBus: QueryBus
    ) { }

    getDragons(): Observable<DragonQueryDocument[]> {
        return this.dragonRepository
            .getDragons()
            .pipe(dragons => dragons);
    }

    async createDragon(createDragonDto: CreateDragonDto): Promise<DragonQueryDocument> {
        return await this.commandBus.execute(
            new CreateDragonCommand(createDragonDto)
        );
    }

    async fetchDragonRequestHandler(data: FetchDragonRequest): Promise<void> {
        const { dragonId, commandId } = data;

        this.queryBus.execute(
            new GetDragonByIdQuery(false, dragonId, commandId, RabbitMQQueues.FETCH_DRAGON_REQUESTED_QUEUE)
        );
    }

    async dragonDiesRequestHandler(data: DragonDiesRequest): Promise<void> {
        const { commandId, dragonId } = data;

        await this.commandBus.execute(
            new DragonDiesRequestCommand(
                dragonId, commandId, RabbitMQQueues.DRAGON_DIES_REQUESTED_QUEUE
            )
        );
    }
}
