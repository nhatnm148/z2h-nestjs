import { Provider } from '@nestjs/common';
import { MongoDBEnums } from 'src/@databases/mongodb/mongodb-collections.enum';
import { DragonQuerySchema } from 'src/dragons/repository/schemas/dragon-query.schema';
import { DragonRepositoryBase } from 'src/dragons/repository/dragon-repository.base';
import { DragonRepository } from 'src/dragons/repository/dragon.repository';
import { DragonQueryDocument } from 'src/dragons/repository/documents/dragon-query.document';
import { Connection, Model } from 'mongoose';
import { DragonCommandSchema } from 'src/dragons/repository/schemas/dragon-command.schema';

export const DragonCollectionsProviders: Provider[] = [
    {
        provide: MongoDBEnums.DRAGON_QUERY_COLLECTION,
        useFactory: (connection: Connection): Model<DragonQueryDocument, {}> => {
            return connection.model(
                MongoDBEnums.DRAGON_QUERY_COLLECTION,
                DragonQuerySchema
            );
        },
        inject: [MongoDBEnums.GAME_CQRS_CONNECTION_TOKEN]
    },
    {
        provide: MongoDBEnums.DRAGON_COMMAND_COLLECTION,
        useFactory: (connection: Connection): Model<DragonQueryDocument, {}> => {
            return connection.model(
                MongoDBEnums.DRAGON_COMMAND_COLLECTION,
                DragonCommandSchema
            );
        },
        inject: [MongoDBEnums.GAME_CQRS_CONNECTION_TOKEN]
    }
];

export const DragonRepositoryProvider: Provider = {
    provide: DragonRepositoryBase,
    useClass: DragonRepository,
    inject: [MongoDBEnums.DRAGON_QUERY_COLLECTION, MongoDBEnums.DRAGON_COMMAND_COLLECTION]
};
