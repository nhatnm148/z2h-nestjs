import { Schema } from 'mongoose';
import { v4 } from 'uuid';
import { LivingStatuses } from 'src/shared/enumrations/living-statuses.enum';

export const DragonQuerySchema = new Schema({
    _id: { type: String, default: v4 },
    name: { type: String },
    stats: {
        hp: { type: Number },
        currentHp: { type: Number },
        strength: { type: Number }
    },
    status: { type: LivingStatuses, default: LivingStatuses.ALIVE }
});
