import { Schema } from 'mongoose';
import { v4 } from 'uuid';
import { CudCommandTypes } from 'src/shared/enumrations/cud-command-types.enum';

export const DragonCommandSchema = new Schema({
    _id: { type: String, default: v4 },
    dragonId: { type: String },
    type: { type: CudCommandTypes },
    date: { type: String },
    payload: { type: Object }
});
