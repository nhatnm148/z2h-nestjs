/* eslint-disable @typescript-eslint/no-unused-vars */
import { DragonQueryDocument } from 'src/dragons/repository/documents/dragon-query.document';
import { Observable } from 'rxjs';
import { CreateDragonDto } from 'src/dragons/dtos/create-dragon.dto';
import { HeroCommandDocument } from 'src/heroes/repository/documents/hero-command.document';
import { DragonCommandDocument } from 'src/dragons/repository/documents/dragon-command.document';

export abstract class DragonRepositoryBase {
    getDragons(): Observable<DragonQueryDocument[]> {
        return null;
    }

    getDragonById(dragonId: string): Observable<DragonQueryDocument> {
        return null;
    }

    createDragon(createDragonDto: CreateDragonDto): Observable<DragonQueryDocument[]> {
        return null;
    }

    goodbyeDragon(dragonId: string): Observable<DragonQueryDocument> {
        return null;
    }

    createCommand(payload: any): Observable<DragonCommandDocument> {
        return null;
    }
}
