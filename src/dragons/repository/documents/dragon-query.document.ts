import { Document } from 'mongoose';
import { LivingStatuses } from 'src/shared/enumrations/living-statuses.enum';
import { IsNumber, IsNotEmpty } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class DragonQueryDocument extends Document {
    name: string;
    stats: Stats;
    status: LivingStatuses;
}

export class Stats {
    @ApiProperty()
    @IsNotEmpty()
    @IsNumber()
    hp: number;

    @ApiProperty()
    @IsNotEmpty()
    @IsNumber()
    currentHp: number;

    @ApiProperty()
    @IsNotEmpty()
    @IsNumber()
    strength: number;
}

