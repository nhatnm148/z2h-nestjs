import { Document } from 'mongoose';
import { CudCommandTypes } from 'src/shared/enumrations/cud-command-types.enum';

export class DragonCommandDocument extends Document {
    type: CudCommandTypes;
    date: string;
    dragonId: string;
    payload: any;
}
