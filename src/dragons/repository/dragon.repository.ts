import { DragonRepositoryBase } from 'src/dragons/repository/dragon-repository.base';
import { Inject } from '@nestjs/common';
import { MongoDBEnums } from 'src/@databases/mongodb/mongodb-collections.enum';
import { Model } from 'mongoose';
import { DragonQueryDocument } from 'src/dragons/repository/documents/dragon-query.document';
import { Observable, from } from 'rxjs';
import { CreateDragonDto } from 'src/dragons/dtos/create-dragon.dto';
import { LivingStatuses } from 'src/shared/enumrations/living-statuses.enum';
import { DragonCommandDocument } from 'src/dragons/repository/documents/dragon-command.document';

export class DragonRepository implements DragonRepositoryBase {
    constructor(
        @Inject(MongoDBEnums.DRAGON_QUERY_COLLECTION)
        private dragonQueryRepository: Model<DragonQueryDocument>,
        @Inject(MongoDBEnums.DRAGON_COMMAND_COLLECTION)
        private dragonCommandRepository: Model<DragonCommandDocument>
    ) { }

    getDragons(): Observable<DragonQueryDocument[]> {
        return from(this.dragonQueryRepository.aggregate([
            {
                $match: {
                    status: LivingStatuses.ALIVE
                }
            },
            {
                $addFields: {
                    id: '$_id'
                }
            },
            {
                $unset: ['_id', '__v']
            }
        ]));
    }

    getDragonById(dragonId: string): Observable<DragonQueryDocument> {
        return from(this.dragonQueryRepository.findOne({ _id: dragonId, status: LivingStatuses.ALIVE }));
    }

    createDragon(createDragonDto: CreateDragonDto): Observable<DragonQueryDocument[]> {
        return from(this.dragonQueryRepository.insertMany([createDragonDto]));
    }

    goodbyeDragon(dragonId: string): Observable<DragonQueryDocument> {
        return from(this.dragonQueryRepository.findByIdAndUpdate(
            dragonId, { status: LivingStatuses.DEAD }, { new: true }
        ));
    }

    // Command
    createCommand(payload: any): Observable<DragonCommandDocument> {
        return from(this.dragonCommandRepository.insertMany(payload));
    }
}
