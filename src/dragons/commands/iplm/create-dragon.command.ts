import { ICommand } from '@nestjs/cqrs';
import { CreateDragonDto } from 'src/dragons/dtos/create-dragon.dto';

export class CreateDragonCommand implements ICommand {
    constructor(
        public createDragonDto: CreateDragonDto
    ) { }
}
