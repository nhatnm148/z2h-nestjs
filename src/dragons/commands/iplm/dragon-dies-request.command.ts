import { ICommand } from '@nestjs/cqrs';

export class DragonDiesRequestCommand implements ICommand {
    constructor(
        public dragonId: string,
        public commandId: string,
        public queueName: string
    ) { }
}
