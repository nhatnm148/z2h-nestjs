import { CommandHandler, ICommandHandler } from '@nestjs/cqrs';
import { CreateDragonCommand } from 'src/dragons/commands/iplm/create-dragon.command';
import { DragonQueryDocument } from 'src/dragons/repository/documents/dragon-query.document';
import { Inject } from '@nestjs/common';
import { DragonRepositoryBase } from 'src/dragons/repository/dragon-repository.base';
import { DragonCommandDocument } from 'src/dragons/repository/documents/dragon-command.document';
import { CudCommandTypes } from 'src/shared/enumrations/cud-command-types.enum';

@CommandHandler(CreateDragonCommand)
export class CreateDragonHandler implements ICommandHandler<CreateDragonCommand> {
    constructor(
        @Inject(DragonRepositoryBase.name)
        private dragonRepositoryBase: DragonRepositoryBase
    ) { }

    async execute(command: CreateDragonCommand): Promise<DragonQueryDocument> {
        const { createDragonDto } = command;

        const createdDragon = await this.dragonRepositoryBase
            .createDragon(createDragonDto)
            .toPromise();

        const eventPayload: Partial<DragonCommandDocument> = {
            dragonId: createdDragon[0]._id,
            type: CudCommandTypes.CREATE,
            date: new Date().toISOString(),
            payload: createdDragon
        };

        await this.dragonRepositoryBase.createCommand(eventPayload);

        return createdDragon[0];
    }
}
