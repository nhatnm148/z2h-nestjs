import { CreateDragonHandler } from 'src/dragons/commands/handlers/create-dragon.handler';
import { DragonDiesRequestHandler } from 'src/dragons/commands/handlers/dragon-dies-request.handler';

export const DragonCommandsHandlers = [CreateDragonHandler, DragonDiesRequestHandler];
