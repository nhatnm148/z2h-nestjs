import { CommandHandler, ICommandHandler, EventBus } from '@nestjs/cqrs';
import { DragonDiesRequestCommand } from 'src/dragons/commands/iplm/dragon-dies-request.command';
import { Inject } from '@nestjs/common';
import { DragonRepositoryBase } from 'src/dragons/repository/dragon-repository.base';
import { DragonCommandDocument } from 'src/dragons/repository/documents/dragon-command.document';
import { CudCommandTypes } from 'src/shared/enumrations/cud-command-types.enum';
import { LivingStatuses } from 'src/shared/enumrations/living-statuses.enum';
import { DragonDiedRequestEvent } from 'src/dragons/events/iplm/dragon-died-request.event';

@CommandHandler(DragonDiesRequestCommand)
export class DragonDiesRequestHandler implements ICommandHandler<DragonDiesRequestCommand> {
    constructor(
        @Inject(DragonRepositoryBase.name)
        private dragonRepository: DragonRepositoryBase,
        private eventBus: EventBus
    ) { }
    
    async execute(command: DragonDiesRequestCommand): Promise<void> {
        const { dragonId, commandId, queueName } = command;

        const eventPayload: Partial<DragonCommandDocument> = {
            dragonId,
            date: new Date().toISOString(),
            type: CudCommandTypes.UPDATE,
            payload: {
                status: LivingStatuses.DEAD
            }
        };

        await this.dragonRepository.createCommand(eventPayload);

        this.eventBus.publish(
            new DragonDiedRequestEvent(dragonId, commandId, queueName)
        );
    }
}