export enum RabbitMQQueues {
    FETCH_HERO_REQUEST_QUEUE = 'z2h-fetch-hero-request-queue',
    FETCH_HERO_REQUESTED_QUEUE = 'z2h-fetch-hero-requested-queue',
    FETCH_DRAGON_REQUEST_QUEUE = 'z2h-fetch-dragon-request-queue',
    FETCH_DRAGON_REQUESTED_QUEUE = 'z2h-fetch-dragon-requested-queue',
    UPDATE_HERO_REQUEST_QUEUE = 'z2h-update-hero-request-queue',
    UPDATE_HERO_REQUESTED_QUEUE = 'z2h-update-hero-requested-queue',
    DRAGON_DIES_REQUEST_QUEUE = 'z2h-dragon-dies-request-queue',
    DRAGON_DIES_REQUESTED_QUEUE = 'z2h-dragon-dies-requested-queue',
    HERO_DIES_REQUEST_QUEUE = 'z2h-hero-dies-request-queue',
    HERO_DIES_REQUESTED_QUEUE = 'z2h-hero-dies-requested-queue'
}

export enum RabbitMQRequestStatuses {
    SUCCEED = 'succeed',
    FAILED = 'failed'
}

export enum RabbitMQEnums {
    RABBIT_MQ_CONNECTION = 'z2h-rabbit_mq_connection'
}
