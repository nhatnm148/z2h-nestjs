import { Provider, Logger } from '@nestjs/common';
import { ConfigService } from 'src/@config/config-manager/config.service';
import * as amqp from 'amqplib';
import { RabbitMQEnums } from 'src/@msg-brokers/rabbitmq/rabbit-mq.enum';

export const RabbitMQConnectionProvider: Provider = {
    provide: RabbitMQEnums.RABBIT_MQ_CONNECTION,
    inject: [ConfigService],
    useFactory: async (configService: ConfigService): Promise<amqp.Channel> => {
        return await amqp.connect(configService.get('RABBITMQ_URL'))
            .then(async (connection) => {
                new Logger().debug('RabbitMQ connected!');

                const queues = ConfigService.getQueues();

                const channelWrapper = await connection.createChannel();

                await channelWrapper.prefetch(1);

                await new Promise((resolve) => {
                    queues.forEach( async (queue: any, index, arr) => {
                        await channelWrapper.assertQueue(queue.name, { durable: true })
                            .then(() => {
                                new Logger().debug(`Connected to queue: ${queue.name} successfully!`);

                                if (index === arr.length - 1) {
                                    resolve();
                                };
                            }).catch(() => {
                                new Logger().debug(`Failed to connect to queue: ${queue.name}!`);
                            });
                    });
                }).then(() => {
                    new Logger().debug('Done connecting to queues!');
                });

                return channelWrapper;
            })
            .catch(() => {
                new Logger().debug('Noooooooo!!!');
                return null;
            });
    }
}
