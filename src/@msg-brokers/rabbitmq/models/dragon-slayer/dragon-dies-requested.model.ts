import { DragonRefDocument } from 'src/shared/models/dragon-ref.document';

export class DragonDiesRequested {
    commandId: string;
    updatedDragon: DragonRefDocument;
}
