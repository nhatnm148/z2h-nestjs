import { HeroRefDocument } from 'src/shared/models/hero-ref.document';

export class UpdateHeroRequested {
    commandId: string;
    updatedHero: HeroRefDocument;
    goldsGot: number;
}
