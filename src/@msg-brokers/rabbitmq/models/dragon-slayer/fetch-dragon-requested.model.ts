import { DragonRefDocument } from 'src/shared/models/dragon-ref.document';
import { RabbitMQRequestStatuses } from 'src/@msg-brokers/rabbitmq/rabbit-mq.enum';

export class FetchDragonRequested {
    commandId: string;
    fetchedDragon: DragonRefDocument;
}
