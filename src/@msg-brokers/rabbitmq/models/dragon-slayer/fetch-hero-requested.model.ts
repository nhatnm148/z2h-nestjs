import { HeroRefDocument } from 'src/shared/models/hero-ref.document'

export class FetchHeroRequested {
    commandId: string;
    fetchedHero: HeroRefDocument;
}
