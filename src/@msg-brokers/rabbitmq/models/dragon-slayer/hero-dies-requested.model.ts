import { HeroRefDocument } from 'src/shared/models/hero-ref.document';

export class HeroDiesRequested {
    commandId: string;
    updatedHero: HeroRefDocument;
}
