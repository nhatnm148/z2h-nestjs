export class HeroDiesRequest {
    commandId: string;
    heroId: string;
}
