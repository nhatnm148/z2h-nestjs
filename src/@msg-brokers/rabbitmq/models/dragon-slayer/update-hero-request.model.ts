import { FightResult } from 'src/shared/models/fight-result.model';
import { HeroRefDocument } from 'src/shared/models/hero-ref.document';

export class UpdateHeroRequest {
    commandId: string;
    hero: HeroRefDocument;
    fightResult: FightResult;
}
