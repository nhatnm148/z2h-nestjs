import { Injectable, Inject } from '@nestjs/common';
import * as amqp from 'amqplib';
import { RabbitMQEnums } from 'src/@msg-brokers/rabbitmq/rabbit-mq.enum';

@Injectable()
export class RabbitMQService {
    constructor(
        @Inject(RabbitMQEnums.RABBIT_MQ_CONNECTION)
        private rabbitMQChannel: amqp.Channel
    ) { }

    get rabbitMQChannelInstance(): amqp.Channel {
        return this.rabbitMQChannel;
    }

    send(queue: string, data: any): void {
        const bufferedData = Buffer.from(JSON.stringify(data));

        this.rabbitMQChannel.sendToQueue(queue, bufferedData);
    }

    ack(data: amqp.ConsumeMessage) {
        this.rabbitMQChannel.ack(data);
    }

    cancel(consumerTag: string): void {
        this.rabbitMQChannel.cancel(consumerTag);
    }

    parseData(data: Buffer): any {
        return JSON.parse(data.toString());
    }

    async assertQueue(queueName: string): Promise<void> {
        await this.rabbitMQChannel.assertQueue(queueName, { durable: true });
    }

    async deleteQueue(queueName: string): Promise<void> {
        await this.rabbitMQChannel.deleteQueue(queueName);
    }
}
