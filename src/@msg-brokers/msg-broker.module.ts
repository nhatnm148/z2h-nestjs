import { Module, Global } from '@nestjs/common';
import { RabbitMQConnectionProvider } from 'src/@msg-brokers/rabbitmq/rabbit-mq-connection.provider';
import { RabbitMQService } from 'src/@msg-brokers/rabbitmq/rabbit-mq.service';

@Global()
@Module({
    providers: [
        RabbitMQConnectionProvider,
        RabbitMQService
    ],
    exports: [
        RabbitMQService
    ]
})
export class MsgBrokerModule { }
