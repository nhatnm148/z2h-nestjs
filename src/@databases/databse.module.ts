import { Module, Global } from '@nestjs/common';
import { MongoDBConnectionsProviders } from 'src/@databases/mongodb/mongodb-connection.provider';

@Global()
@Module({
    providers: [
        ...MongoDBConnectionsProviders
    ],
    exports: [
        ...MongoDBConnectionsProviders
    ]
})
export class DatabaseModule { }
