import { Provider } from '@nestjs/common';
import { MongoDBEnums } from 'src/@databases/mongodb/mongodb-collections.enum';
import * as mongoose from 'mongoose';
import { ConfigService } from 'src/@config/config-manager/config.service';

const mongoDBConfigs: mongoose.ConnectionOptions = {
    useFindAndModify: false,
    useNewUrlParser: true,
    useUnifiedTopology: true
};

export const MongoDBConnectionsProviders: Provider[] = [
    {
        provide: MongoDBEnums.BOOK_CONNECTION_TOKEN,
        inject: [ConfigService],
        useFactory: async (configService: ConfigService): Promise<mongoose.Connection> => {
            return await mongoose.createConnection(
                configService.get('MONGODB_URL'),
                mongoDBConfigs
            );
        }
    },
    {
        provide: MongoDBEnums.GAME_CQRS_CONNECTION_TOKEN,
        inject: [ConfigService],
        useFactory: async (configService: ConfigService): Promise<mongoose.Connection> => {
            return await mongoose.createConnection(
                configService.get('GAME_CQRS_MONGODB_URL'),
                mongoDBConfigs
            );
        }
    },
    {
        provide: MongoDBEnums.GAME_EVENT_CQRS_CONNECTION_TOKEN,
        inject: [ConfigService],
        useFactory: async (configService: ConfigService): Promise<mongoose.Connection> => {
            return await mongoose.createConnection(
                configService.get('MONGODB_URL'),
                mongoDBConfigs
            );
        }
    },
    {
        provide: MongoDBEnums.MESSENGER_CONNECTION_TOKEN,
        inject: [ConfigService],
        useFactory: async (configService: ConfigService): Promise<mongoose.Connection> => {
            return await mongoose.createConnection(
                configService.get('MESSENGER_MONGODB_URL'),
                mongoDBConfigs
            );
        }
    }
];
