export enum MongoDBEnums {
    // Books database
    BOOK_CONNECTION_TOKEN = 'book_mongodb_connection',
    BOOK_COLLECTION = 'books',
    
    // Games database
    GAME_CQRS_CONNECTION_TOKEN = 'game-cqrs-connection-token',
    GAME_EVENT_CQRS_CONNECTION_TOKEN = 'game-event-cqrs-connection-token',
    HERO_QUERY_COLLECTION = 'heroes-queries',
    HERO_COMMAND_COLLECTION = 'heroes-commands',
    DRAGON_QUERY_COLLECTION = 'dragons-queries',
    DRAGON_COMMAND_COLLECTION = 'dragons-commands',
    
    // Game-events database
    DRAGON_SLAYER_EVENT_CQRS_COLLECTION = 'dragon-slayer-commands',

    // Messenger database
    MESSENGER_CONNECTION_TOKEN = 'messenger-connection-token',
    USER_QUERY_COLLECTION = 'user-queries',
    USER_COMMAND_COLLECTION = 'user-commands',
    MESSAGE_QUERY_COLLECTION = 'message-queries'
}
