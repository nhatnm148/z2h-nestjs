export enum CudCommandTypes {
    CREATE = 'create',
    UPDATE = 'update',
    DELETE = 'delete'
}
