export enum LivingStatuses {
    ALIVE = 'alive',
    DEAD = 'dead'
}
