import { Injectable } from '@nestjs/common';
import { Document } from 'mongoose';

@Injectable()
export class RespondBodyFilterService {
    // eslint-disable-next-line @typescript-eslint/ban-types
    transform<Object extends Document>(document: Object): Object {
        if (!document) {
            return null;
        }

        const res = document.toObject();

        res.id = res._id;
        delete res._id;
        delete res.__v;

        return res;
    }
}
