export class FightResult {
    isHeroWins: boolean;
    isHeroDead: boolean;
    hpLost: number;
}
