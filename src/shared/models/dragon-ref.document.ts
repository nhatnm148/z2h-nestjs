import { Document } from 'mongoose';
import { LivingStatuses } from 'src/shared/enumrations/living-statuses.enum';

export class DragonRefDocument extends Document {
    name: string;
    stats: Stats;
    status: LivingStatuses;
}

export class Stats {
    hp: number;
    currentHp: number;
    strength: number;
}

