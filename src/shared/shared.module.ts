import { Module } from '@nestjs/common';
import { RespondBodyFilterService } from 'src/shared/services/respond-body-filter.service';
import { CqrsModule } from '@nestjs/cqrs';

@Module({
    imports: [
        CqrsModule
    ],
    providers: [
        RespondBodyFilterService
    ],
    exports: [
        CqrsModule,
        RespondBodyFilterService
    ]
})
export class SharedModule { }
