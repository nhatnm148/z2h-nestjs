import { Injectable, NotFoundException } from '@nestjs/common';
import { CommandBus, QueryBus } from '@nestjs/cqrs';
import { CreateUserCommand } from 'src/messenger-users/commands/iplm/create-user.command';
import { CreateUserDto } from 'src/messenger-users/dtos/create-user.dto';
import { SearchUsersFilterDto } from 'src/messenger-users/dtos/search-users-filter.dto';
import { UserQueryDocument } from 'src/messenger-users/repositories/documents/user-query.document';
import { SearchUsersQuery } from 'src/messenger-users/queries/iplm/search-users.query';
import { LoginQuery } from 'src/messenger-users/queries/iplm/login.query';
import { UserCredentialsDto } from 'src/messenger-users/dtos/user-credentials.dto';

@Injectable()
export class MessengerUsersService {
    constructor(
        private commandBus: CommandBus,
        private queryBus: QueryBus
    ) { }

    async searchUsers(filter: SearchUsersFilterDto): Promise<UserQueryDocument[]> {
        return this.queryBus.execute(
            new SearchUsersQuery(filter)
        );
    }

    async login(payload: UserCredentialsDto): Promise<Partial<UserQueryDocument>> {
        return this.queryBus
            .execute(new LoginQuery(payload))
            .then((user: Partial<UserQueryDocument>) => {
                const res: Partial<UserQueryDocument> = {
                    id: user._id,
                    name: user.name,
                    username: user.username
                };

                return res;
            })
            .catch(err => {
                throw new NotFoundException(err);
            });
    }

    async createUser(payload: CreateUserDto): Promise<void> {
        await this.commandBus.execute(
            new CreateUserCommand(payload)
        );
    }
}
