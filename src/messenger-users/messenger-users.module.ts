import { Module } from '@nestjs/common';
import { MessengerUsersController } from './messenger-users.controller';
import { MessengerUsersService } from './messenger-users.service';
import { UserCollectionsProvider, UserRepositoryProvider } from 'src/messenger-users/repositories/user.providers';
import { CqrsModule } from '@nestjs/cqrs';
import { messengerCommandHandlers } from 'src/messenger-users/commands/handlers/messenger-users.command.handlers';
import { messengerUserQueryHandlers } from 'src/messenger-users/queries/handlers/messenger-user.query.handlers';

@Module({
  imports: [
    CqrsModule
  ],
  controllers: [
    MessengerUsersController
  ],
  providers: [
    MessengerUsersService,
    UserRepositoryProvider,
    ...UserCollectionsProvider,
    ...messengerCommandHandlers,
    ...messengerUserQueryHandlers
  ]
})
export class MessengerUsersModule {}
