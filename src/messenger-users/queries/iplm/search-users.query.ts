import { IQuery } from '@nestjs/cqrs';
import { SearchUsersFilterDto } from 'src/messenger-users/dtos/search-users-filter.dto';

export class SearchUsersQuery implements IQuery {
    constructor(
        public filter: SearchUsersFilterDto
    ) { }
}
