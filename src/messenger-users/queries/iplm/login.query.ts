import { IQuery } from '@nestjs/cqrs';
import { UserCredentialsDto } from 'src/messenger-users/dtos/user-credentials.dto';

export class LoginQuery implements IQuery {
    constructor(
        public payload: UserCredentialsDto
    ) { }
}
