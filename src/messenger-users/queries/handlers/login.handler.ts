import { QueryHandler, IQueryHandler } from '@nestjs/cqrs';
import { LoginQuery } from 'src/messenger-users/queries/iplm/login.query';
import { Inject } from '@nestjs/common';
import { UserRepositoryBase } from 'src/messenger-users/repositories/user-repository.base';
import { plainToClass } from 'class-transformer';
import { UserCredentialsDto } from 'src/messenger-users/dtos/user-credentials.dto';

@QueryHandler(LoginQuery)
export class LoginHandler implements IQueryHandler<LoginQuery> {
    constructor(
        @Inject(UserRepositoryBase.name)
        private userRepository: UserRepositoryBase
    ) { }

    async execute(query: LoginQuery): Promise<any> {
        return new Promise((resolve, reject) => {
            this.userRepository
                .getUser(query.payload)
                .then(async (user) => {
                    if (!user) {
                        reject('User not found!');
                        return;
                    }

                    const userModelRef = plainToClass(UserCredentialsDto, query.payload);

                    if (await userModelRef.validatePassword(user)) {
                        resolve({
                            _id: user._id,
                            name: user.name,
                            username: user.username
                        });
                    }

                    reject('Invalid password!');
                });
        });
    }
}
