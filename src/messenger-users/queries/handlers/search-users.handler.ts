import { QueryHandler, IQueryHandler } from '@nestjs/cqrs';
import { SearchUsersQuery } from 'src/messenger-users/queries/iplm/search-users.query';
import { Inject } from '@nestjs/common';
import { UserRepositoryBase } from 'src/messenger-users/repositories/user-repository.base';
import { UserQueryDocument } from 'src/messenger-users/repositories/documents/user-query.document';

@QueryHandler(SearchUsersQuery)
export class SearchUsersHandler implements IQueryHandler<SearchUsersQuery> {
    constructor(
        @Inject(UserRepositoryBase.name)
        private userRepository: UserRepositoryBase
    ) { }

    async execute(query: SearchUsersQuery): Promise<Partial<UserQueryDocument>[]> {
        return this.userRepository
            .searchUsers(query.filter)
            .then(users => users.map(user => {
                return { name: user.name, username: user.username };
            }));
    }
}
