import { SearchUsersHandler } from 'src/messenger-users/queries/handlers/search-users.handler';
import { LoginHandler } from 'src/messenger-users/queries/handlers/login.handler';

export const messengerUserQueryHandlers = [SearchUsersHandler, LoginHandler];