import { ICommand } from '@nestjs/cqrs';
import { CreateUserDto } from 'src/messenger-users/dtos/create-user.dto';

export class CreateUserCommand implements ICommand {
    constructor(
        public payload: CreateUserDto
    ) { }
}
