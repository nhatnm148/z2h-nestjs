import { CommandHandler, ICommandHandler } from '@nestjs/cqrs';
import { CreateUserCommand } from 'src/messenger-users/commands/iplm/create-user.command';
import { Inject } from '@nestjs/common';
import { UserRepositoryBase } from 'src/messenger-users/repositories/user-repository.base';
import { plainToClass } from 'class-transformer';
import { CreateUserDto } from 'src/messenger-users/dtos/create-user.dto';

@CommandHandler(CreateUserCommand)
export class CreateUserHandler implements ICommandHandler<CreateUserCommand> {
    constructor(
        @Inject(UserRepositoryBase.name)
        private userRepository: UserRepositoryBase
    ) { }
    
    async execute(command: CreateUserCommand): Promise<any> {
        const createUserDtoRef = plainToClass(CreateUserDto, command.payload);
        const userPayload = await createUserDtoRef.prepareUserToCreate(command.payload);

        await this.userRepository.createUser(userPayload);
    }
}
