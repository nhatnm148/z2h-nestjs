import { CreateUserHandler } from 'src/messenger-users/commands/handlers/create-user.handler';

export const messengerCommandHandlers = [CreateUserHandler];