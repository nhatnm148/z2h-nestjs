import { UserRepositoryBase } from 'src/messenger-users/repositories/user-repository.base';
import { MongoDBEnums } from 'src/@databases/mongodb/mongodb-collections.enum';
import { UserQueryDocument } from 'src/messenger-users/repositories/documents/user-query.document';
import { Model } from 'mongoose';
import { Inject } from '@nestjs/common';
import { SearchUsersFilterDto } from 'src/messenger-users/dtos/search-users-filter.dto';
import { UserCredentialsDto } from 'src/messenger-users/dtos/user-credentials.dto';

export class UserRepository implements UserRepositoryBase {
    constructor(
        @Inject(MongoDBEnums.USER_QUERY_COLLECTION)
        private userQueryRepository: Model<UserQueryDocument>
    ) { }

    async getUser(userCredentials: UserCredentialsDto): Promise<UserQueryDocument> {
        return this.userQueryRepository.findOne({ username: userCredentials.username });
    }

    async searchUsers(filter: SearchUsersFilterDto): Promise<UserQueryDocument[]> {
        const { name } = filter;
        
        return this.userQueryRepository.aggregate([
            {
                $match: {
                    $or: [
                        { name: { $regex: `.*${name}.*`, $options: 'i' } },
                        { username: { $regex: `.*${name}.*`, $options: 'i' } }
                    ]
                }
            },
            {
                $limit: 3
            }
        ]);
    }

    async createUser(createUserDto: Partial<UserQueryDocument>): Promise<UserQueryDocument> {
        return this.userQueryRepository
            .insertMany(createUserDto)
            .then(createdUsers => createdUsers[0]);
    }
}
