import { Document } from 'mongoose';
import { CudCommandTypes } from 'src/shared/enumrations/cud-command-types.enum';

export class UserCommandDocument extends Document {
    userId: string;
    type: CudCommandTypes;
    date: string;
    payload: any;
}
