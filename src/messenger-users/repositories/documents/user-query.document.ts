import { Document } from 'mongoose';

export class UserQueryDocument extends Document {
    name: string;
    username: string;
    password: string;
    salt: string;
}
