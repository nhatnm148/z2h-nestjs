/* eslint-disable @typescript-eslint/no-unused-vars */
import { UserQueryDocument } from 'src/messenger-users/repositories/documents/user-query.document';
import { SearchUsersFilterDto } from 'src/messenger-users/dtos/search-users-filter.dto';
import { UserCredentialsDto } from 'src/messenger-users/dtos/user-credentials.dto';

export abstract class UserRepositoryBase {
    async getUser(userCredentials: UserCredentialsDto): Promise<UserQueryDocument> {
        return null;
    }

    async searchUsers(filter: SearchUsersFilterDto): Promise<UserQueryDocument[]> {
        return null;
    }

    async createUser(createUserDto: Partial<UserQueryDocument>): Promise<UserQueryDocument> {
        return null;
    }
}
