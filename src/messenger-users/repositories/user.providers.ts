import { Provider } from '@nestjs/common';
import { MongoDBEnums } from 'src/@databases/mongodb/mongodb-collections.enum';
import { Connection, Model } from 'mongoose';
import { UserQueryDocument } from 'src/messenger-users/repositories/documents/user-query.document';
import { UserQuerySchema } from 'src/messenger-users/repositories/schemas/user-query.schema';
import { UserCommandDocument } from 'src/messenger-users/repositories/documents/user-command.document';
import { UserCommandSchema } from 'src/messenger-users/repositories/schemas/user-command.schema';
import { UserRepositoryBase } from 'src/messenger-users/repositories/user-repository.base';
import { UserRepository } from 'src/messenger-users/repositories/user.repository';

export const UserCollectionsProvider: Provider[] = [
    {
        provide: MongoDBEnums.USER_QUERY_COLLECTION,
        useFactory: (connection: Connection): Model<UserQueryDocument, {}> => {
            return connection.model(
                MongoDBEnums.USER_QUERY_COLLECTION,
                UserQuerySchema
            );
        },
        inject: [MongoDBEnums.MESSENGER_CONNECTION_TOKEN]
    },
    {
        provide: MongoDBEnums.USER_COMMAND_COLLECTION,
        useFactory: (connection: Connection): Model<UserCommandDocument, {}> => {
            return connection.model(
                MongoDBEnums.USER_COMMAND_COLLECTION,
                UserCommandSchema
            );
        },
        inject: [MongoDBEnums.MESSENGER_CONNECTION_TOKEN]
    }
];

export const UserRepositoryProvider: Provider = {
    provide: UserRepositoryBase,
    useClass: UserRepository,
    inject: [MongoDBEnums.USER_QUERY_COLLECTION, MongoDBEnums.USER_COMMAND_COLLECTION]
}
