import { Schema } from 'mongoose';
import { v4 } from 'uuid';
import { CudCommandTypes } from 'src/shared/enumrations/cud-command-types.enum';

export const UserCommandSchema = new Schema({
    _id: { type: String, default: v4 },
    userId: { type: String },
    type: { type: CudCommandTypes },
    date: { type: String }
});
