import { Schema } from 'mongoose';
import { v4 } from 'uuid';

export const UserQuerySchema = new Schema({
    _id: { type: String, default: v4 },
    name: { type: String },
    username: { type: String },
    password: { type: String },
    salt: { type: String }
});
