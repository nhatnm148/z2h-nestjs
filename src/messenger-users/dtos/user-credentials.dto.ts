import { IsNotEmpty } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';
import * as bcrypt from 'bcrypt';
import { UserQueryDocument } from 'src/messenger-users/repositories/documents/user-query.document';

export class UserCredentialsDto {
    @ApiProperty()
    @IsNotEmpty()
    username: string;

    @ApiProperty()
    @IsNotEmpty()
    password: string;

    async validatePassword(user: UserQueryDocument): Promise<boolean> {
        return user.password === await bcrypt.hash(this.password, user.salt);
    }
}
