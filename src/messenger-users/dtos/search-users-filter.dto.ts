import { IsNotEmpty } from 'class-validator';

export class SearchUsersFilterDto {
    @IsNotEmpty()
    name: string;
}
