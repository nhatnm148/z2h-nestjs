import { IsNotEmpty, IsString } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';
import { UserQueryDocument } from 'src/messenger-users/repositories/documents/user-query.document';
import * as bcrypt from 'bcrypt';

export class CreateUserDto {
    @ApiProperty()
    @IsNotEmpty()
    @IsString()
    name: string;

    @ApiProperty()
    @IsNotEmpty()
    @IsString()
    username: string;

    @ApiProperty()
    @IsNotEmpty()
    @IsString()
    password: string;

    async prepareUserToCreate(payload: CreateUserDto): Promise<Partial<UserQueryDocument>> {
        const preparedPayload: Partial<UserQueryDocument> = payload;
        const salt = await bcrypt.genSalt();

        preparedPayload.password = await bcrypt.hash(payload.password, salt);
        preparedPayload.salt = salt;

        return preparedPayload;
    }
}
