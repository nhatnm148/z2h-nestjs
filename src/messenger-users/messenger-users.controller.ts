import { Controller, Post, Body, ValidationPipe, Get, Param } from '@nestjs/common';
import { CreateUserDto } from 'src/messenger-users/dtos/create-user.dto';
import { MessengerUsersService } from 'src/messenger-users/messenger-users.service';
import { UserQueryDocument } from 'src/messenger-users/repositories/documents/user-query.document';
import { UserCredentialsDto } from 'src/messenger-users/dtos/user-credentials.dto';

@Controller()
export class MessengerUsersController {
    constructor(
        private messengerUsersService: MessengerUsersService
    ) { }

    @Get('/:name')
    async searchUsers(
        @Param('name') name: string 
    ): Promise<UserQueryDocument[]> {
        return await this.messengerUsersService.searchUsers({ name });
    }

    @Post('/login')
    async login(
        @Body(ValidationPipe) payload: UserCredentialsDto
    ): Promise<any> {
        return this.messengerUsersService.login(payload);
    }

    @Post()
    async createUser(
        @Body(ValidationPipe) payload: CreateUserDto
    ): Promise<void> {
        await this.messengerUsersService.createUser(payload);
    }
}
