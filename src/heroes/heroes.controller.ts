import { Controller, Post, Body, Get, Param } from '@nestjs/common';
import { CreateHeroDto } from 'src/heroes/dtos/create-hero.dto';
import { Observable } from 'rxjs';
import { HeroQueryDocument } from 'src/heroes/repository/documents/hero-query.document';
import { HeroesService } from 'src/heroes/heroes.service';

@Controller()
export class HeroesController {
    constructor(
        private heroesService: HeroesService
    ) { }

    @Get()
    getHeroes(): Observable<HeroQueryDocument[]> {
        return this.heroesService.getHeroes();
    }

    @Get(':id')
    async getHeroeById(
        @Param('id') heroId: string
    ): Promise<HeroQueryDocument> {
        return await this.heroesService.fetchHeroHandler(false, {
            commandId: null,
            heroId
        });
    }

    @Post()
    async createHero(
        @Body() createHeroDto: CreateHeroDto
    ): Promise<any> {
        return await this.heroesService.createHero(createHeroDto);
    }
}
