import { Injectable } from '@nestjs/common';
import { RabbitMQService } from 'src/@msg-brokers/rabbitmq/rabbit-mq.service';
import { RabbitMQQueues } from 'src/@msg-brokers/rabbitmq/rabbit-mq.enum';
import { FetchHeroRequest } from 'src/@msg-brokers/rabbitmq/models/dragon-slayer/fetch-hero-request.model';
import { HeroesService } from 'src/heroes/heroes.service';
import { UpdateHeroRequest } from 'src/@msg-brokers/rabbitmq/models/dragon-slayer/update-hero-request.model';
import { HeroDiesRequest } from 'src/@msg-brokers/rabbitmq/models/dragon-slayer/hero-dies-request.model';

@Injectable()
export class HeroesEventsHandler {
    constructor(
        private rabbitMQService: RabbitMQService,
        private heroesService: HeroesService
    ) {
        this.registerToQueues$();
    }

    registerToQueues$(): void {
        const rmqChannel = this.rabbitMQService.rabbitMQChannelInstance;

        // Fetch hero request listener
        rmqChannel.consume(
            RabbitMQQueues.FETCH_HERO_REQUEST_QUEUE,
            data => {
                this.fetchHeroRequestHandler(this.rabbitMQService.parseData(data.content));
                this.rabbitMQService.ack(data);
            }
        );

        rmqChannel.consume(
            RabbitMQQueues.UPDATE_HERO_REQUEST_QUEUE,
            data => {
                this.updateHeroRequestHandler(this.rabbitMQService.parseData(data.content));
                this.rabbitMQService.ack(data);
            }
        );

        rmqChannel.consume(
            RabbitMQQueues.HERO_DIES_REQUEST_QUEUE,
            data => {
                this.heroDiesRequestHandler(this.rabbitMQService.parseData(data.content));
                this.rabbitMQService.ack(data);
            }
        )
    }

    fetchHeroRequestHandler(data: FetchHeroRequest): void {
        console.log('received', data.commandId);
        this.heroesService.fetchHeroHandler(false, data);
    }

    updateHeroRequestHandler(data: UpdateHeroRequest): void {
        this.heroesService.updateHeroRequestHandler(data);
    }

    heroDiesRequestHandler(data: HeroDiesRequest): void {
        this.heroesService.heroDiesRequestHandler(data);
    }
}
