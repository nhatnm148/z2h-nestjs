import { Document } from 'mongoose';
import { LivingStatuses } from 'src/shared/enumrations/living-statuses.enum';

export interface HeroQueryDocument extends Document {
    name: string;
    stats: Stats;
    golds: number;
    status: LivingStatuses;
}

export class Stats {
    hp: number;
    currentHp: number;
    strength: number;
}
