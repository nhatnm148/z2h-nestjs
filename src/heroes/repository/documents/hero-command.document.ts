import { Document } from 'mongoose';
import { CudCommandTypes } from 'src/shared/enumrations/cud-command-types.enum';

export interface HeroCommandDocument extends Document {
    type: CudCommandTypes;
    date: string;
    heroId: string;
    payload: any;
}
