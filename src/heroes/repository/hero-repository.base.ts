/* eslint-disable @typescript-eslint/no-unused-vars */
import { HeroQueryDocument } from 'src/heroes/repository/documents/hero-query.document';
import { Observable } from 'rxjs';
import { CreateHeroDto } from 'src/heroes/dtos/create-hero.dto';
import { HeroCommandDocument } from 'src/heroes/repository/documents/hero-command.document';

export abstract class HeroRepositoryBase {
    // Query
    getHeroes(): Observable<HeroQueryDocument[]> {
        return null;
    }

    getHeroById(heroId: string): Observable<HeroQueryDocument> {
        return null;
    }

    createHero(createHeroDto: CreateHeroDto): Observable<HeroQueryDocument[]> {
        return null;
    }

    updateHero(
        heroId: string,
        updatePayload: Partial<HeroQueryDocument>
    ): Observable<HeroQueryDocument> {
        return null;
    }

    goodbyeHero(heroId: string): Observable<HeroQueryDocument> {
        return null;
    }

    // Command
    createCommand(payload: any): Observable<HeroCommandDocument> {
        return null;
    }
}
