import { HeroRepositoryBase } from 'src/heroes/repository/hero-repository.base';
import { HeroQueryDocument } from 'src/heroes/repository/documents/hero-query.document';
import { Inject } from '@nestjs/common';
import { MongoDBEnums } from 'src/@databases/mongodb/mongodb-collections.enum';
import { Model } from 'mongoose';
import { CreateHeroDto } from 'src/heroes/dtos/create-hero.dto';
import { Observable, from } from 'rxjs';
import { HeroCommandDocument } from 'src/heroes/repository/documents/hero-command.document';
import { LivingStatuses } from 'src/shared/enumrations/living-statuses.enum';

export class HeroRepository implements HeroRepositoryBase {
    constructor(
        @Inject(MongoDBEnums.HERO_QUERY_COLLECTION)
        private heroQueryRepository: Model<HeroQueryDocument>,
        @Inject(MongoDBEnums.HERO_COMMAND_COLLECTION)
        private heroCommandRepository: Model<HeroCommandDocument>
    ) { }

    // Query collection
    getHeroes(): Observable<HeroQueryDocument[]> {
        return from(this.heroQueryRepository.aggregate([
            {
                $match: {
                    status: LivingStatuses.ALIVE
                }
            },
            {
                $addFields: {
                    id: '$_id'
                }
            },
            {
                $unset: ['_id', '__v']
            }
        ]));
    }

    getHeroById(heroId: string): Observable<HeroQueryDocument> {
        return from(this.heroQueryRepository.findOne({ _id: heroId }));
    }

    createHero(createHeroDto: CreateHeroDto): Observable<HeroQueryDocument[]> {
        return from(this.heroQueryRepository.insertMany([createHeroDto]));
    }

    updateHero(
        heroId: string,
        updatePayload: Partial<HeroQueryDocument>
    ): Observable<HeroQueryDocument> {
        return from(this.heroQueryRepository.findByIdAndUpdate(
            heroId,
            updatePayload,
            { new: true }
        ));
    }

    goodbyeHero(heroId: string): Observable<HeroQueryDocument> {
        return from(
            this.heroQueryRepository
                .findByIdAndUpdate(heroId, { status: LivingStatuses.DEAD }, { new: true })
        );
    }

    // Command
    createCommand(payload: any): Observable<HeroCommandDocument> {
        return from(this.heroCommandRepository.insertMany(payload));
    }
}
