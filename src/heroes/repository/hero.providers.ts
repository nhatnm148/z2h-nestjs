import { Provider } from '@nestjs/common';
import { MongoDBEnums } from 'src/@databases/mongodb/mongodb-collections.enum';
import { Connection, Model } from 'mongoose';
import { HeroQueryDocument } from 'src/heroes/repository/documents/hero-query.document';
import { HeroSchema } from 'src/heroes/repository/schemas/hero-query.schema';
import { HeroRepositoryBase } from 'src/heroes/repository/hero-repository.base';
import { HeroRepository } from 'src/heroes/repository/hero.repository';
import { HeroCommandDocument } from 'src/heroes/repository/documents/hero-command.document';
import { HeroCommandSchema } from 'src/heroes/repository/schemas/hero-command.schema';

export const HeroCollectionsProviders: Provider[] = [
    {
        provide: MongoDBEnums.HERO_QUERY_COLLECTION,
        useFactory: (connection: Connection): Model<HeroQueryDocument, {}> => {
            return connection.model(
                MongoDBEnums.HERO_QUERY_COLLECTION,
                HeroSchema
            );
        },
        inject: [MongoDBEnums.GAME_CQRS_CONNECTION_TOKEN]
    },
    {
        provide: MongoDBEnums.HERO_COMMAND_COLLECTION,
        useFactory: (connection: Connection): Model<HeroCommandDocument, {}> => {
            return connection.model(
                MongoDBEnums.HERO_COMMAND_COLLECTION,
                HeroCommandSchema
            );
        },
        inject: [MongoDBEnums.GAME_CQRS_CONNECTION_TOKEN]
    }
];

export const HeroRepositoryProvider: Provider = {
    provide: HeroRepositoryBase,
    useClass: HeroRepository,
    inject: [MongoDBEnums.HERO_QUERY_COLLECTION, MongoDBEnums.HERO_COMMAND_COLLECTION]
}
