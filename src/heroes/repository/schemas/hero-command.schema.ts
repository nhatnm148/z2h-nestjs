import { Schema } from 'mongoose';
import { v4 } from 'uuid';
import { CudCommandTypes } from 'src/shared/enumrations/cud-command-types.enum';

export const HeroCommandSchema = new Schema({
    _id: { type: String, default: v4 },
    type: { type: CudCommandTypes },
    date: { type: String },
    heroId: { type: String },
    payload: { type: Object }
});
