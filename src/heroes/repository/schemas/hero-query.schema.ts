import { Schema } from 'mongoose';
import { v4 } from 'uuid';
import { LivingStatuses } from 'src/shared/enumrations/living-statuses.enum';

export const HeroSchema = new Schema({
    _id: { type: String, default: v4 },
    name: { type: String, default: 'Guest' },
    stats: {
        hp: { type: Number, default: 100 },
        currentHp: { type: Number, default: 100 },
        strength: { type: Number, default: 20 }
    },
    golds: { type: Number, default: 10 },
    status: { type: LivingStatuses, default: LivingStatuses.ALIVE }
});
