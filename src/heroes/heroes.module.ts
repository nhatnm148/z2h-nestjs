import { Module } from '@nestjs/common';
import { HeroesService } from 'src/heroes/heroes.service';
import { HeroesController } from 'src/heroes/heroes.controller';
import { HeroCollectionsProviders, HeroRepositoryProvider } from 'src/heroes/repository/hero.providers';
import { RespondBodyFilterService } from 'src/shared/services/respond-body-filter.service';
import { HeroCommandsHandlers } from 'src/heroes/commands/handlers/hero-commands.handlers';
import { HeroEventsHandlers } from 'src/heroes/events/handlers/hero-events.handlers';
import { CqrsModule } from '@nestjs/cqrs';
import { HeroesEventsHandler } from 'src/heroes/heroes-events.handler';
import { HeroQueriesHandlers } from 'src/heroes/queries/handlers/hero-queries.handlers';

@Module({
  imports: [
    CqrsModule
  ],
  providers: [
    HeroesService,
    ...HeroCollectionsProviders,
    HeroRepositoryProvider,
    HeroesEventsHandler,
    RespondBodyFilterService,
    ...HeroCommandsHandlers,
    ...HeroEventsHandlers,
    ...HeroQueriesHandlers
  ],
  controllers: [
    HeroesController
  ]
})
export class HeroesModule { }
