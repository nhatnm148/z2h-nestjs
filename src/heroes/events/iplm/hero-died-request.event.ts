import { IEvent } from '@nestjs/cqrs';

export class HeroDiedRequestEvent implements IEvent {
    constructor(
        public heroId: string,
        public commandId: string,
        public queueName: string
    ) { }
}
