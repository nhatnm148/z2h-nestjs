import { IEvent } from '@nestjs/cqrs';
import { FightResult } from 'src/shared/models/fight-result.model';
import { HeroRefDocument } from 'src/shared/models/hero-ref.document';

export class UpdatedHeroRequestEvent implements IEvent {
    constructor(
        public hero: HeroRefDocument,
        public updatePayload: FightResult,
        public goldsGot: number,
        public commandId: string,
        public queueName: string
    ) { }
}
