import { IEvent } from '@nestjs/cqrs';
import { FetchHeroRequested } from 'src/@msg-brokers/rabbitmq/models/dragon-slayer/fetch-hero-requested.model';

export class GotHeroByIdEvent implements IEvent {
    constructor(
        public queueName: string,
        public payload: FetchHeroRequested
    ) { }
}
