import { HeroDiedRequestHandler } from 'src/heroes/events/handlers/hero-died-request.handler';
import { GotHeroByIdHandler } from 'src/heroes/events/handlers/got-hero-by-id.handler';
import { UpdatedHeroRequestHandler } from 'src/heroes/events/handlers/updated-hero-request.handler';

export const HeroEventsHandlers = [
    GotHeroByIdHandler,
    UpdatedHeroRequestHandler,
    HeroDiedRequestHandler
];
