import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { UpdatedHeroRequestEvent } from 'src/heroes/events/iplm/updated-hero-request.event';
import { RabbitMQService } from 'src/@msg-brokers/rabbitmq/rabbit-mq.service';
import { Inject } from '@nestjs/common';
import { HeroRepositoryBase } from 'src/heroes/repository/hero-repository.base';
import { UpdateHeroRequested } from 'src/@msg-brokers/rabbitmq/models/dragon-slayer/update-hero-requested.model';
import { HeroRefDocument } from 'src/shared/models/hero-ref.document';

@EventsHandler(UpdatedHeroRequestEvent)
export class UpdatedHeroRequestHandler implements IEventHandler<UpdatedHeroRequestEvent> {
    constructor(
        @Inject(HeroRepositoryBase.name)
        private heroRepositoryBase: HeroRepositoryBase,
        private rabbitMQService: RabbitMQService
    ) { }

    async handle(event: UpdatedHeroRequestEvent): Promise<void> {
        const {
            hero, updatePayload,
            commandId, queueName, goldsGot
        } = event;

        hero.golds += goldsGot;
        hero.stats.currentHp -= updatePayload.hpLost;
        const updatedHero: HeroRefDocument = await this.heroRepositoryBase
            .updateHero(hero._id, hero)
            .toPromise();

        const subQueueName = `${queueName}.${commandId}`;
        const payload: UpdateHeroRequested = {
            commandId,
            updatedHero,
            goldsGot
        }

        this.rabbitMQService.send(subQueueName, payload);
    }
}
