import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { GotHeroByIdEvent } from 'src/heroes/events/iplm/got-hero-by-id.event';
import { RabbitMQService } from 'src/@msg-brokers/rabbitmq/rabbit-mq.service';

@EventsHandler(GotHeroByIdEvent)
export class GotHeroByIdHandler implements IEventHandler {
    constructor(
        private rabbitMQService: RabbitMQService
    ) { }

    handle(event: GotHeroByIdEvent): void {
        const { queueName, payload } = event;

        this.rabbitMQService.send(queueName, payload);
    }
}
