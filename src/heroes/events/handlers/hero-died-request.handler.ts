import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { HeroDiedRequestEvent } from 'src/heroes/events/iplm/hero-died-request.event';
import { Inject } from '@nestjs/common';
import { HeroRepositoryBase } from 'src/heroes/repository/hero-repository.base';
import { RabbitMQService } from 'src/@msg-brokers/rabbitmq/rabbit-mq.service';
import { HeroDiesRequested } from 'src/@msg-brokers/rabbitmq/models/dragon-slayer/hero-dies-requested.model';

@EventsHandler(HeroDiedRequestEvent)
export class HeroDiedRequestHandler implements IEventHandler<HeroDiedRequestEvent> {
    constructor(
        @Inject(HeroRepositoryBase.name)
        private heroRepository: HeroRepositoryBase,
        private rabbitMQService: RabbitMQService
    ) { }

    async handle(event: HeroDiedRequestEvent): Promise<void> {
        const { heroId, commandId, queueName } = event;

        const updatedHero = await this.heroRepository
            .goodbyeHero(heroId)
            .toPromise();

        const subQueueName = `${queueName}.${commandId}`;
        const payload: HeroDiesRequested = {
            commandId,
            updatedHero
        };

        this.rabbitMQService.send(subQueueName, payload);
    }
}
