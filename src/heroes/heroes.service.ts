import { Injectable, Inject } from '@nestjs/common';
import { HeroRepositoryBase } from 'src/heroes/repository/hero-repository.base';
import { CreateHeroDto } from 'src/heroes/dtos/create-hero.dto';
import { HeroQueryDocument } from 'src/heroes/repository/documents/hero-query.document';
import { Observable } from 'rxjs';
import { CommandBus, QueryBus } from '@nestjs/cqrs';
import { FetchHeroRequest } from 'src/@msg-brokers/rabbitmq/models/dragon-slayer/fetch-hero-request.model';
import { RabbitMQService } from 'src/@msg-brokers/rabbitmq/rabbit-mq.service';
import { RabbitMQQueues } from 'src/@msg-brokers/rabbitmq/rabbit-mq.enum';
import { UpdateHeroRequest } from 'src/@msg-brokers/rabbitmq/models/dragon-slayer/update-hero-request.model';
import { CreateHeroCommand } from 'src/heroes/commands/iplm/create-hero.command';
import { GetHeroByIdQuery } from 'src/heroes/queries/iplm/get-hero-by-id.query';
import { UpdateHeroRequestCommand } from 'src/heroes/commands/iplm/update-hero-request.command';
import { HeroDiesRequestCommand } from 'src/heroes/commands/iplm/hero-dies-request.command';

@Injectable()
export class HeroesService {
    constructor(
        @Inject(HeroRepositoryBase.name)
        private heroRepository: HeroRepositoryBase,
        private commandBus: CommandBus,
        private queryBus: QueryBus,
        private rabbitMQService: RabbitMQService
    ) { }

    getHeroes(): Observable<HeroQueryDocument[]> {
        return this.heroRepository
            .getHeroes()
            .pipe(res => res);
    }

    async createHero(createHeroDto: CreateHeroDto): Promise<any> {
        return await this.commandBus.execute(
            new CreateHeroCommand(createHeroDto)
        );
    }

    async fetchHeroHandler(isInternal: boolean, data: FetchHeroRequest): Promise<HeroQueryDocument> {
        const { commandId, heroId } = data;

        return await this.queryBus.execute(
            new GetHeroByIdQuery(
                isInternal, heroId, commandId, RabbitMQQueues.FETCH_HERO_REQUESTED_QUEUE
            )
        );
    }

    async updateHeroRequestHandler(data: UpdateHeroRequest): Promise<void> {
        const { commandId, hero, fightResult } = data;

        await this.commandBus.execute(
            new UpdateHeroRequestCommand(
                hero, fightResult, commandId, RabbitMQQueues.UPDATE_HERO_REQUESTED_QUEUE
            )
        );
    }

    async heroDiesRequestHandler(data): Promise<void> {
        const { commandId, heroId } = data;

        await this.commandBus.execute(
            new HeroDiesRequestCommand(
                heroId, commandId, RabbitMQQueues.HERO_DIES_REQUESTED_QUEUE
            )
        );
    }
}
