import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsNotEmptyObject } from 'class-validator';
import { Stats } from 'src/heroes/repository/documents/hero-query.document';

export class CreateHeroDto {
    @ApiProperty()
    @IsNotEmpty()
    name: string;
}
