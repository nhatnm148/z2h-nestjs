import { ICommand } from '@nestjs/cqrs';
import { FightResult } from 'src/shared/models/fight-result.model';
import { HeroRefDocument } from 'src/shared/models/hero-ref.document';

export class UpdateHeroRequestCommand implements ICommand {
    constructor(
        public hero: HeroRefDocument,
        public updatePayload: FightResult,
        public commandId: string,
        public queueName: string
    ) { }
}
