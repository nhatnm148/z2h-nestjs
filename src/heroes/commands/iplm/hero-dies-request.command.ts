import { ICommand } from '@nestjs/cqrs';

export class HeroDiesRequestCommand implements ICommand {
    constructor(
        public heroId: string,
        public commandId: string,
        public queueName: string
    ) { }
}
