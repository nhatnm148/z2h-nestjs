import { CreateHeroDto } from 'src/heroes/dtos/create-hero.dto';
import { ICommand } from '@nestjs/cqrs';

export class CreateHeroCommand implements ICommand {
    constructor(
        public createHeroDto: CreateHeroDto
    ) { }
}
