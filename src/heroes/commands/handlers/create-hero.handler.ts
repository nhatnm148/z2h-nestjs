import { CommandHandler, ICommandHandler } from '@nestjs/cqrs';
import { CreateHeroCommand } from 'src/heroes/commands/iplm/create-hero.command';
import { HeroRepositoryBase } from 'src/heroes/repository/hero-repository.base';
import { Inject } from '@nestjs/common';
import { CudCommandTypes } from 'src/shared/enumrations/cud-command-types.enum';
import { HeroCommandDocument } from 'src/heroes/repository/documents/hero-command.document';
import { HeroQueryDocument } from 'src/heroes/repository/documents/hero-query.document';

@CommandHandler(CreateHeroCommand)
export class CreateHeroHandler implements ICommandHandler<CreateHeroCommand> {
    constructor(
        @Inject(HeroRepositoryBase.name)
        private heroRepository: HeroRepositoryBase
    ) { }

    async execute(command: CreateHeroCommand): Promise<HeroQueryDocument> {
        const { createHeroDto } = command;

        const createdHero = await this.heroRepository
            .createHero(createHeroDto)
            .toPromise();

        const eventPayload: Partial<HeroCommandDocument> = {
            type: CudCommandTypes.CREATE,
            date: new Date().toISOString(),
            heroId: createdHero[0]._id,
            payload: createdHero[0]
        };

        await this.heroRepository.createCommand(eventPayload);

        return createdHero[0];
    }
}
