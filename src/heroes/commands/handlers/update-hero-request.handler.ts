import { CommandHandler, ICommandHandler, EventBus } from '@nestjs/cqrs';
import { UpdateHeroRequestCommand } from 'src/heroes/commands/iplm/update-hero-request.command';
import { Inject } from '@nestjs/common';
import { HeroRepositoryBase } from 'src/heroes/repository/hero-repository.base';
import { CudCommandTypes } from 'src/shared/enumrations/cud-command-types.enum';
import { HeroCommandDocument } from 'src/heroes/repository/documents/hero-command.document';
import { UpdatedHeroRequestEvent } from 'src/heroes/events/iplm/updated-hero-request.event';

@CommandHandler(UpdateHeroRequestCommand)
export class UpdateHeroRequestHandler implements ICommandHandler<UpdateHeroRequestCommand> {
    constructor(
        @Inject(HeroRepositoryBase.name)
        private heroRepository: HeroRepositoryBase,
        private eventBus: EventBus
    ) { }

    async execute(command: UpdateHeroRequestCommand): Promise<void> {
        const { hero, updatePayload, commandId, queueName } = command;
        const goldsGot = (1 + Math.floor((Math.random() * 9)));

        const eventPayload: Partial<HeroCommandDocument> = {
            heroId: hero._id,
            date: new Date().toISOString(),
            type: CudCommandTypes.UPDATE,
            payload: {
                goldsGot,
                hpLost: updatePayload.hpLost
            }
        };

        await this.heroRepository.createCommand({ ...eventPayload });

        this.eventBus.publish(
            new UpdatedHeroRequestEvent(
                hero, updatePayload, goldsGot, commandId, queueName
            )
        );
    }
}
