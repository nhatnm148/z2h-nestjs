import { CommandHandler, ICommandHandler, EventBus } from '@nestjs/cqrs';
import { HeroDiesRequestCommand } from 'src/heroes/commands/iplm/hero-dies-request.command';
import { HeroRepositoryBase } from 'src/heroes/repository/hero-repository.base';
import { HeroCommandDocument } from 'src/heroes/repository/documents/hero-command.document';
import { Inject } from '@nestjs/common';
import { CudCommandTypes } from 'src/shared/enumrations/cud-command-types.enum';
import { LivingStatuses } from 'src/shared/enumrations/living-statuses.enum';
import { HeroDiedRequestEvent } from 'src/heroes/events/iplm/hero-died-request.event';

@CommandHandler(HeroDiesRequestCommand)
export class HeroDiesRequestHandler implements ICommandHandler<HeroDiesRequestCommand> {
    constructor(
        @Inject(HeroRepositoryBase.name)
        private heroRepository: HeroRepositoryBase,
        private eventBus: EventBus
    ) { }

    async execute(command: HeroDiesRequestCommand): Promise<void> {
        const { heroId, commandId, queueName } = command;
        const eventPayload: Partial<HeroCommandDocument> = {
            type: CudCommandTypes.UPDATE,
            date: new Date().toISOString(),
            heroId,
            payload: {
                status: LivingStatuses.DEAD
            }
        };

        await this.heroRepository
            .createCommand(eventPayload)
            .toPromise();

        this.eventBus.publish(
            new HeroDiedRequestEvent(heroId, commandId, queueName)
        );
    }
}
