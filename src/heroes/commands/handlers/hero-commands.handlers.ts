import { CreateHeroHandler } from 'src/heroes/commands/handlers/create-hero.handler';
import { UpdateHeroRequestHandler } from 'src/heroes/commands/handlers/update-hero-request.handler';
import { HeroDiesRequestHandler } from 'src/heroes/commands/handlers/hero-dies-request.handler';

export const HeroCommandsHandlers = [
    CreateHeroHandler,
    UpdateHeroRequestHandler,
    HeroDiesRequestHandler
];
