import { IQuery } from '@nestjs/cqrs';

export class GetHeroByIdQuery implements IQuery {
    constructor(
        public isInternal: boolean,
        public heroId: string,
        public commandId: string,
        public queueName?: string
    ) { }
}
