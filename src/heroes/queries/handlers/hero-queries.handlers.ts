import { GetHeroByIdHandler } from 'src/heroes/queries/handlers/get-hero-by-id.handler';

export const HeroQueriesHandlers = [GetHeroByIdHandler]