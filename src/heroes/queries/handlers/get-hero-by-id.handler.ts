import { QueryHandler, IQueryHandler, EventBus } from '@nestjs/cqrs';
import { GetHeroByIdQuery } from 'src/heroes/queries/iplm/get-hero-by-id.query';
import { Inject } from '@nestjs/common';
import { HeroRepositoryBase } from 'src/heroes/repository/hero-repository.base';
import { FetchHeroRequested } from 'src/@msg-brokers/rabbitmq/models/dragon-slayer/fetch-hero-requested.model';
import { GotHeroByIdEvent } from 'src/heroes/events/iplm/got-hero-by-id.event';
import { HeroRefDocument } from 'src/shared/models/hero-ref.document';

@QueryHandler(GetHeroByIdQuery)
export class GetHeroByIdHandler implements IQueryHandler<GetHeroByIdQuery> {
    constructor(
        @Inject(HeroRepositoryBase.name)
        private heroRepository: HeroRepositoryBase,
        private eventBus: EventBus
    ) { }

    async execute(query: GetHeroByIdQuery): Promise<any> {
        const { commandId, queueName, isInternal, heroId } = query;

        const fetchedHero: HeroRefDocument = await this.heroRepository
            .getHeroById(heroId)
            .toPromise();

        if (!isInternal) {
            const subQueueName = `${queueName}.${commandId}`;
            const payload: FetchHeroRequested = {
                commandId,
                fetchedHero
            };

            this.eventBus.publish(
                new GotHeroByIdEvent(subQueueName, payload)
            );
        }

        return fetchedHero;
    }
}
