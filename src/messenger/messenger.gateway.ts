import { WebSocketGateway, WebSocketServer, SubscribeMessage } from '@nestjs/websockets';
import { MessengerService } from 'src/messenger/messenger.service';

@WebSocketGateway()
export class MessengerGateway {
    @WebSocketServer() server: any;

    constructor(
        private messengerService: MessengerService
    ) { }

    @SubscribeMessage('receive')
    handleMessage(_, msg) {
        msg.message.date = new Date().toISOString();
        
        this.server.emit(`${msg.from}`, msg);
        this.server.emit(`${msg.to}`, msg);

        this.messengerService.createMessage(msg);
    }
}
