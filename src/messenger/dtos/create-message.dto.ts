import { IsNotEmpty } from 'class-validator';
import { Message } from 'src/messenger/repositories/schemas/message-query.schema';
import { ApiProperty } from '@nestjs/swagger';

export class CreateMessageDto {
    @ApiProperty()
    @IsNotEmpty()
    from: string;

    @ApiProperty()
    @IsNotEmpty()
    to: string;

    @ApiProperty()
    @IsNotEmpty()
    message: Message;
}
