/* eslint-disable @typescript-eslint/no-unused-vars */
import { MessageQueryDocument } from 'src/messenger/repositories/documents/message-query.document';
import { MessengerRepositoryBase } from 'src/messenger/repositories/mesenger.repository.base';
import { Inject } from '@nestjs/common';
import { MongoDBEnums } from 'src/@databases/mongodb/mongodb-collections.enum';
import { Model } from 'mongoose';
import { CreateMessageDto } from 'src/messenger/dtos/create-message.dto';
import { v4 } from 'uuid';
import { UserMessage } from 'src/messenger/models/user-message.model';

export class MessengerRepository implements MessengerRepositoryBase {
    constructor(
        @Inject(MongoDBEnums.MESSAGE_QUERY_COLLECTION)
        private messageQueryRepository: Model<MessageQueryDocument>
    ) { }

    async getHistories(from: string): Promise<UserMessage[]> {
        return await this.messageQueryRepository.aggregate([
            {
                $match: {
                    users: { $elemMatch: { $eq: from } },
                }
            },
            {
                $lookup: {
                    from: MongoDBEnums.USER_QUERY_COLLECTION,
                    let: { users: '$users' },
                    pipeline: [
                        {
                            $match: {
                                $expr: { $in: ['$_id', '$$users'] }
                            }
                        },
                        {
                            $project: {
                                username: 1,
                                name: 1,
                                id: '$_id'
                            }
                        },
                        {
                            $unset: '_id'
                        }
                    ],
                    as: 'userRefs'
                }
            },
            {
                $project: {
                    id: '$_id',
                    from: {
                        $arrayElemAt: [
                            {
                                $filter: {
                                    input: '$userRefs',
                                    as: 'userRef',
                                    cond: { $eq: ['$$userRef.id', from] }
                                }
                            },
                            0
                        ]
                    },
                    to: {
                        $arrayElemAt: [
                            {
                                $filter: {
                                    input: '$userRefs',
                                    as: 'userRef',
                                    cond: { $ne: ['$$userRef.id', from] }
                                }
                            },
                            0
                        ]
                    },
                    messages: 1,
                    lastMsg: 1
                }
            },
            {
                $sort: { lastMsg: -1 }
            },
            {
                $unset: '_id'
            }
        ]);
    }

    async getHistory(from: string, to: string): Promise<MessageQueryDocument> {
        return await this.messageQueryRepository.findOne({
            $and: [
                { $in: [from, '$users'] },
                { $in: [to, '$users'] }
            ]
        });
    }

    async createMessage(createMsgDto: CreateMessageDto): Promise<void> {
        const lastMsg = new Date().toISOString();
        const { from, to, message } = createMsgDto;
        message.date = new Date().toISOString();

        await this.messageQueryRepository.updateOne(
            {
                users: {
                    $all: [
                        { $elemMatch: { $eq: from } },
                        { $elemMatch: { $eq: to } }
                    ]
                }
            },
            {
                lastMsg,
                $push: {
                    messages: {
                        $each: [message],
                        $position: 0
                    }
                },
                $setOnInsert: {
                    _id: v4(),
                    users: [from, to]
                }
            },
            { upsert: true }
        );
    }
}
