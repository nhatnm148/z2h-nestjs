import { Schema } from 'mongoose';
import { v4 } from 'uuid';
import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty } from 'class-validator';

export class Message {
    date: string;

    @ApiProperty()
    @IsNotEmpty()
    content: string;

    @ApiProperty()
    @IsNotEmpty()
    sender: string;
} 

export const MessageQuerySchema = new Schema({
    _id: { type: String, default: v4 },
    users: { type: Array },
    messages: { type: Array },
    lastMsg: { type: String, default: new Date().toISOString() }
});
