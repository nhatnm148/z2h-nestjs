import { Provider } from '@nestjs/common';
import { MongoDBEnums } from 'src/@databases/mongodb/mongodb-collections.enum';
import { Connection, Model } from 'mongoose';
import { MessageQueryDocument } from 'src/messenger/repositories/documents/message-query.document';
import { MessageQuerySchema } from 'src/messenger/repositories/schemas/message-query.schema';
import { MessengerRepositoryBase } from 'src/messenger/repositories/mesenger.repository.base';
import { MessengerRepository } from 'src/messenger/repositories/mesenger.repository';

export const messengerCollectionsProviders: Provider[] = [
    {
        provide: MongoDBEnums.MESSAGE_QUERY_COLLECTION,
        useFactory: (connection: Connection): Model<MessageQueryDocument, {}> => {
            return connection.model(
                MongoDBEnums.MESSAGE_QUERY_COLLECTION,
                MessageQuerySchema
            );
        },
        inject: [MongoDBEnums.MESSENGER_CONNECTION_TOKEN]
    }
];

export const messengerRepositoryProvider: Provider = {
    provide: MessengerRepositoryBase,
    useClass: MessengerRepository,
    inject: [MongoDBEnums.MESSAGE_QUERY_COLLECTION]
}
