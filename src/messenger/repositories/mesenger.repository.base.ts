/* eslint-disable @typescript-eslint/no-unused-vars */
import { MessageQueryDocument } from 'src/messenger/repositories/documents/message-query.document';
import { CreateMessageDto } from 'src/messenger/dtos/create-message.dto';
import { UserMessage } from 'src/messenger/models/user-message.model';

export abstract class MessengerRepositoryBase {
    async getHistories(from: string): Promise<UserMessage[]> {
        return null;
    }

    async getHistory(from: string, to: string): Promise<MessageQueryDocument> {
        return null;
    }

    async createMessage(createMsgDto: CreateMessageDto): Promise<void> {
        return null;
    }
}
