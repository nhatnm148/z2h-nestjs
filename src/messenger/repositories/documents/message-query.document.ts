import { Document } from 'mongoose';
import { Message } from 'src/messenger/repositories/schemas/message-query.schema';

export class MessageQueryDocument extends Document {
    users: string[];
    messages: Message[];
}