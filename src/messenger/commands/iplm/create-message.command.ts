import { ICommand } from '@nestjs/cqrs';
import { CreateMessageDto } from 'src/messenger/dtos/create-message.dto';

export class CreateMessageCommand implements ICommand {
    constructor(
        public payload: CreateMessageDto
    ) { }
}
