import { CommandHandler, ICommandHandler } from '@nestjs/cqrs';
import { CreateMessageCommand } from 'src/messenger/commands/iplm/create-message.command';
import { Inject } from '@nestjs/common';
import { MessengerRepositoryBase } from 'src/messenger/repositories/mesenger.repository.base';

@CommandHandler(CreateMessageCommand)
export class CreateMessageHandler implements ICommandHandler<CreateMessageCommand> {
    constructor(
        @Inject(MessengerRepositoryBase.name)
        private messengerRepository: MessengerRepositoryBase
    ) { }

    async execute(command: CreateMessageCommand): Promise<any> {
        return this.messengerRepository.createMessage(command.payload);
    }
}
