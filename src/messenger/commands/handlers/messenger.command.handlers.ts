import { CreateMessageHandler } from 'src/messenger/commands/handlers/create-message.handler';

export const messengerCommandHandlers = [CreateMessageHandler];
