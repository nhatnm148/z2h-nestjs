import { Controller, Post, Body, ValidationPipe, Get, Param } from '@nestjs/common';
import { MessengerService } from 'src/messenger/messenger.service';
import { CreateMessageDto } from 'src/messenger/dtos/create-message.dto';
import { UserMessage } from 'src/messenger/models/user-message.model';

@Controller()
export class MessengerController {
    constructor(
        private messengerService: MessengerService
    ) { }

    @Get('/histories/:userId')
    async getHistories(
        @Param('userId') userId: string
    ): Promise<UserMessage[]> {
        return this.messengerService.getHistories(userId);
    }

    @Post()
    async createMessage(
        @Body(ValidationPipe) payload: CreateMessageDto
    ): Promise<any> {
        return await this.messengerService.createMessage(payload);
    }
}
