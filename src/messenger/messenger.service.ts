import { Injectable } from '@nestjs/common';
import { CommandBus, QueryBus } from '@nestjs/cqrs';
import { CreateMessageDto } from 'src/messenger/dtos/create-message.dto';
import { CreateMessageCommand } from 'src/messenger/commands/iplm/create-message.command';
import { UserMessage } from 'src/messenger/models/user-message.model';
import { GetHistoriesQuery } from 'src/messenger/queries/iplm/get-histories.query';

@Injectable()
export class MessengerService {
    constructor(
        private commandBus: CommandBus,
        private queryBus: QueryBus
    ) { }

    async getHistories(userId: string): Promise<UserMessage[]> {
        return this.queryBus.execute(
            new GetHistoriesQuery(userId)
        );
    }

    async createMessage(payload: CreateMessageDto): Promise<any> {
        return this.commandBus.execute(
            new CreateMessageCommand(payload)
        );
    }
}
