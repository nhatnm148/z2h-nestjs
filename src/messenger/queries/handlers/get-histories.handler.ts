import { QueryHandler, IQueryHandler } from '@nestjs/cqrs';
import { GetHistoriesQuery } from 'src/messenger/queries/iplm/get-histories.query';
import { MessageQueryDocument } from 'src/messenger/repositories/documents/message-query.document';
import { Inject } from '@nestjs/common';
import { MessengerRepositoryBase } from 'src/messenger/repositories/mesenger.repository.base';
import { UserMessage } from 'src/messenger/models/user-message.model';

@QueryHandler(GetHistoriesQuery)
export class GetHistoriesHandler implements IQueryHandler<GetHistoriesQuery> {
    constructor(
        @Inject(MessengerRepositoryBase.name)
        private essengerRepository: MessengerRepositoryBase
    ) { }
    
    async execute(query: GetHistoriesQuery): Promise<UserMessage[]> {
        return await this.essengerRepository.getHistories(query.userId);   
    }
}
