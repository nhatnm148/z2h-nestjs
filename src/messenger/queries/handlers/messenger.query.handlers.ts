import { GetHistoriesHandler } from 'src/messenger/queries/handlers/get-histories.handler';

export const MessengerQueryHandlers = [GetHistoriesHandler];
