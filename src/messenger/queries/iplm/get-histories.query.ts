import { IQuery } from '@nestjs/cqrs';

export class GetHistoriesQuery implements IQuery {
    constructor(
        public userId: string
    ) { }
}
