import { User } from 'src/messenger/models/user-message.model';

export class UserMessageHistory {
    id: string;
    from: User;
    to: User;
    messages: [
        {
            "content": "Let's go and get another Nations League!!",
            "sender": "d8ddadc1-4fd9-482f-a1b2-24306c0f70eb",
            "date": "2020-09-04T09:20:03.797Z"
        }
    ]
}
