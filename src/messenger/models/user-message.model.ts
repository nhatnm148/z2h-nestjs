import { Message } from 'src/messenger/repositories/schemas/message-query.schema';

export class UserMessage {
    id: string;
    from: User;
    to: User;
    messages: Message[];
}

export class User {
    id: string;
    name: string;
    username: string;
}
