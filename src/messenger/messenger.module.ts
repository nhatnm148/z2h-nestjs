import { Module } from '@nestjs/common';
import { MessengerController } from './messenger.controller';
import { MessengerService } from './messenger.service';
import { MessengerGateway } from 'src/messenger/messenger.gateway';
import { SharedModule } from 'src/shared/shared.module';
import { messengerCommandHandlers } from 'src/messenger/commands/handlers/messenger.command.handlers';
import { messengerRepositoryProvider, messengerCollectionsProviders } from 'src/messenger/repositories/messenger.providers';
import { MessengerQueryHandlers } from 'src/messenger/queries/handlers/messenger.query.handlers';

@Module({
  imports: [
    SharedModule
  ],
  controllers: [
    MessengerController
  ],
  providers: [
    MessengerService,
    MessengerGateway,
    messengerRepositoryProvider,
    ...messengerCommandHandlers,
    ...MessengerQueryHandlers,
    ...messengerCollectionsProviders
  ]
})
export class MessengerModule {}
