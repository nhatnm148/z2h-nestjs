import { Module } from '@nestjs/common';
import { BooksService } from './books.service';
import { BooksController } from './books.controller';
import { SharedModule } from 'src/shared/shared.module';
import { BookCollectionProvider, BookRepositoryProvider } from 'src/books/repositories/book.provider';
import { PrometheusModule } from '@willsoto/nestjs-prometheus';

@Module({
  imports: [
    SharedModule,
    PrometheusModule.register({
      path: 'hello'
    })
  ],
  providers: [
    BooksService,
    BookCollectionProvider,
    BookRepositoryProvider
  ],
  controllers: [
    BooksController
  ]
})
export class BooksModule { }
