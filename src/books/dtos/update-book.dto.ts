import { IsNotEmpty, IsOptional } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class UpdateBookDto {
    @ApiProperty()
    @IsOptional()
    @IsNotEmpty()
    title: string;

    @ApiProperty()
    @IsOptional()
    @IsNotEmpty()
    author: string;
}
