import { Document } from 'mongoose';

export class Book extends Document {
    id: string;
    title: string;
    author: string;
}
