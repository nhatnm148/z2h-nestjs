import { Injectable, Inject, InternalServerErrorException, Logger, Scope } from '@nestjs/common';
import { CreateBookDto } from 'src/books/dtos/create-book.dto';
import { Book } from 'src/books/models/book.model';
import { BookRepositoryBase } from 'src/books/repositories/book-repository.base';
import { UpdateBookDto } from 'src/books/dtos/update-book.dto';
import { RespondBodyFilterService } from 'src/shared/services/respond-body-filter.service';
import { MachineOptions, AnyEventObject, interpret, Machine } from 'xstate';
import { BooksEvents, FetchSucceed, FetchFailed, FetchRequested, CreateSucceed, CreateFailed, CreateRequested, UpdateSucceed, UpdateFailed, UpdateRequested, DeleteSucceed, DeleteFailed, DeleteRequested } from 'src/books/@xstate/books.events';
import { Subject, Observable, from, of } from 'rxjs';
import { map, catchError, first } from 'rxjs/operators';
import { IBooksSchema, booksConfig } from 'src/books/@xstate/books.config';

@Injectable({ scope: Scope.REQUEST })
export class BooksService {
    private fetchedBooks$ = new Subject<Book[]>();
    private createdBook$ = new Subject<Book>();
    private updatedBook$ = new Subject<Book>();
    private deletedBook$ = new Subject<Book>();
    private machine;
    private service;
    private booksOptions: Partial<MachineOptions<any, BooksEvents>> = {
        actions: {
            showMessage: (_, event: AnyEventObject) => {
                new Logger().debug(event.msg);
            },
            fetchSucceedProcess: (_, event: FetchSucceed) => {
                this.fetchedBooks$.next(event.books);
            },
            fetchFailedProcess: (_, event: FetchFailed) => {
                this.fetchedBooks$.error(event.msg);
            },
            createSucceedProcess: (_, event: CreateSucceed) => {
                this.createdBook$.next(event.createdBook);
            },
            createFailedProcess: (_, event: CreateFailed) => {
                this.createdBook$.error(event.msg);
            },
            updateSucceedProcess: (_, event: UpdateSucceed) => {
                this.updatedBook$.next(event.updatedBook);
            },
            updateFailedProcess: (_, event: UpdateFailed) => {
                this.updatedBook$.error(event.msg);
            },
            deleteSucceedProcess: () => {
                this.deletedBook$.next(null);
            },
            deleteFailedProcess: (_, event: DeleteFailed) => {
                this.deletedBook$.error(event.msg);
            }
        },
        services: {
            fetchBooks: (_, event: FetchRequested) => {
                return this.fetchBooksHandler(event);
            },
            createBook: (_, event: CreateRequested) => {
                return this.createBookHandler(event);
            },
            updateBook: (_, event: UpdateRequested) => {
                return this.updateBookHandler(event);
            },
            deleteBook: (_, event: DeleteRequested) => {
                return this.deleteBookHandler(event);
            }
        }
    }

    constructor(
        @Inject(BookRepositoryBase.name)
        private bookRepository: BookRepositoryBase,
        private respondBodyFilterService: RespondBodyFilterService
    ) { }

    private initScript(): void {
        const logger = new Logger();

        this.machine = Machine<any, IBooksSchema>(booksConfig).withConfig(this.booksOptions);
        this.service = interpret(this.machine).start();
        const state$ = from(this.service);
        state$.subscribe((state: any) => {
            logger.debug('---------------------');
            logger.debug(`Event sent: ${state?.transitions[0]?.event || ''}`);
            logger.debug(`Current state: ${state.value}`);
            logger.debug('---------------------');

            if (state.value === 'done') {
                this.service.stop();
                logger.debug('Scripts has been stopped!');
            }
        });
    }

    private navigateState(event: AnyEventObject): void {
        this.initScript();
        this.transition(event);
    }

    private transition(event: AnyEventObject): void {
        this.service.send(event);
    }

    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    getBooks(filterDTO?: any): Observable<Book[]> {
        this.navigateState(new FetchRequested());

        this.fetchedBooks$.next([]);

        return this.registerToFirstResult(this.fetchedBooks$);
    }

    createBook(createBookDto: CreateBookDto): Observable<Book> {
        this.navigateState(new CreateRequested(createBookDto));

        this.createdBook$.next(null);

        return this.registerToFirstResult(this.createdBook$);
    }

    updateBook(id: string, updateBookDto: Partial<UpdateBookDto>): Observable<Book> {
        this.navigateState(new UpdateRequested(id, updateBookDto));

        this.updatedBook$.next(null);

        return this.registerToFirstResult(this.updatedBook$);
    }

    async deleteBook(id: string): Promise<void> {
        this.navigateState(new DeleteRequested(id));

        this.deletedBook$.next(null);

        this.registerToFirstResult(this.deletedBook$);
    }

    private registerToFirstResult(observable: Observable<any>): Observable<any> {
        return observable
            .pipe(
                first(),
                map(res => {
                    return res;
                }),
                catchError(err => {
                    throw new InternalServerErrorException(err);
                })
            );
    }

    private fetchBooksHandler(event: FetchRequested): Observable<AnyEventObject> {
        return from(this.bookRepository.getBooks(event?.filterDto || null))
            .pipe(
                map(books => {
                    return new FetchSucceed(books, 'Fetched successfully!');
                }),
                catchError(() => {
                    return of(new FetchFailed('Failed to fetch!'));
                })
            )
    }  

    private createBookHandler(event: CreateRequested): Observable<AnyEventObject> {
        return from(this.bookRepository.createBook(event.createBookDto))
            .pipe(
                map(createdBook => {
                    const res = this.respondBodyFilterService.transform(createdBook);

                    return new CreateSucceed(res, 'Created successfully!');
                }),
                catchError(() => {
                    return of(new CreateFailed('Failed to create!'));
                })
            )
    }

    private updateBookHandler(event: UpdateRequested): Observable<AnyEventObject> {
        const { id, updateBookDto } = event;

        return from(this.bookRepository.updateBook(id, updateBookDto))
            .pipe(
                map(updatedBook => {
                    const res = this.respondBodyFilterService.transform(updatedBook);

                    return new UpdateSucceed(res, 'Update successfully!');
                }),
                catchError(() => {
                    return of(new UpdateFailed('Failed to update!'));
                })
            );
    }

    private deleteBookHandler(event: DeleteRequested): Observable<AnyEventObject> {
        return from(this.bookRepository.deleteBook(event.id))
            .pipe(
                map(() => {
                    return new DeleteSucceed('Delete successfully!');
                }),
                catchError(() => {
                    return of(new DeleteFailed('Failed to delete!'));
                })
            );
    }
}
