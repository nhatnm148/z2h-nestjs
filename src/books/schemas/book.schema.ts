import { Schema } from 'mongoose';
import { v4 } from 'uuid';

export const BookSchema = new Schema({
    _id: { type: String, default: v4 },
    title: { type: String },
    author: { type: String }
});
