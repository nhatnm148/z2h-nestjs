import { Controller, Post, Body, Get, Put, Param, ParseUUIDPipe, Delete, ValidationPipe } from '@nestjs/common';
import { CreateBookDto } from 'src/books/dtos/create-book.dto';
import { Book } from 'src/books/models/book.model';
import { BooksService } from 'src/books/books.service';
import { Observable } from 'rxjs';
import { UpdateBookDto } from 'src/books/dtos/update-book.dto';

@Controller()
export class BooksController {
    constructor(
        private booksService: BooksService
    ) { }

    @Get()
    getBooks(): Observable<Book[]> {
        return this.booksService.getBooks();
    }

    @Post()
    createBook(
        @Body(ValidationPipe) createBookDto: CreateBookDto
    ): Observable<Book> {
        return this.booksService.createBook(createBookDto);
    }

    @Put('/:id')
    updateBook(
        @Body() updateBookDto: UpdateBookDto,
        @Param('id', ParseUUIDPipe) id: string
    ): Observable<Book> {
        Object.keys(updateBookDto).forEach(key => {
            if (!updateBookDto[key]) {
                delete updateBookDto[key];
            }
        });

        return this.booksService.updateBook(id, updateBookDto);
    }

    @Delete('/:id')
    async deleteBook(
        @Param('id', ParseUUIDPipe) id: string
    ): Promise<void> {
        await this.booksService.deleteBook(id);
    }
}
