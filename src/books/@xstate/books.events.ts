import { EventObject } from 'xstate';
import { Book } from 'src/books/models/book.model';
import { CreateBookDto } from 'src/books/dtos/create-book.dto';
import { UpdateBookDto } from 'src/books/dtos/update-book.dto';

export class FetchRequested implements EventObject {
    static readonly type = '[BOOKS] Fetch Requested';
    readonly type = FetchRequested.type;

    constructor(public filterDto?: any) { }
}

export class FetchSucceed implements EventObject {
    static readonly type = '[BOOKS] Fetch Succeed';
    readonly type = FetchSucceed.type;

    constructor(
        public books: Book[],
        public msg: string
    ) { }
}

export class FetchFailed implements EventObject {
    static readonly type = '[BOOKS] Fetch Faile';
    readonly type = FetchFailed.type;

    constructor(public msg: string) { }
}

export class CreateRequested implements EventObject {
    static readonly type = '[BOOKS] Create Requested';
    readonly type = CreateRequested.type;

    constructor(public createBookDto: CreateBookDto) { }
}

export class CreateSucceed implements EventObject {
    static readonly type = '[BOOKS] Create Succeed';
    readonly type = CreateSucceed.type;

    constructor(
        public createdBook: Book,
        public msg: string
    ) { }
}

export class CreateFailed implements EventObject {
    static readonly type = '[BOOKS] Create Faile';
    readonly type = CreateFailed.type;

    constructor(public msg: string) { }
}

export class UpdateRequested implements EventObject {
    static readonly type = '[BOOKS] Update Requested';
    readonly type = UpdateRequested.type;

    constructor(
        public id: string,
        public updateBookDto: Partial<UpdateBookDto>
    ) { }
}

export class UpdateSucceed implements EventObject {
    static readonly type = '[BOOKS] Update Succeed';
    readonly type = UpdateSucceed.type;

    constructor(
        public updatedBook: Book,
        public msg: string
    ) { }
}

export class UpdateFailed implements EventObject {
    static readonly type = '[BOOKS] Update Failed';
    readonly type = UpdateFailed.type;

    constructor(public msg: string) { }
}

export class DeleteRequested implements EventObject {
    static readonly type = '[BOOKS] Delete Requested';
    readonly type = DeleteRequested.type;

    constructor(public id: string) { }
}

export class DeleteSucceed implements EventObject {
    static readonly type = '[BOOKS] Delete Succeed';
    readonly type = DeleteSucceed.type;

    constructor(public msg: string) { }
}

export class DeleteFailed implements EventObject {
    static readonly type = '[BOOKS] Delete Faile';
    readonly type = DeleteFailed.type;

    constructor(public msg: string) { }
}

export type BooksEvents = FetchRequested | FetchSucceed | FetchFailed |
    CreateRequested | CreateSucceed | CreateFailed |
    UpdateRequested | UpdateSucceed | UpdateFailed |
    DeleteRequested | DeleteSucceed | DeleteFailed ;
