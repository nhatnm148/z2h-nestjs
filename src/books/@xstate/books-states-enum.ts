export enum BooksStates {
    IDLE = 'idle',
    FETCH_PROCESSING = 'fetch_processing',
    CREATE_PROCESSING = 'create_processing',
    UPDATE_PROCESSING = 'update_processing',
    DELETE_PROCESSING = 'delete_processing',
    DONE = 'done'
}
