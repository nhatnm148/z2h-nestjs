import { MachineConfig } from 'xstate'; 
import { BooksEvents, FetchRequested, FetchSucceed, FetchFailed, CreateRequested, CreateSucceed, CreateFailed, UpdateRequested, UpdateSucceed, UpdateFailed, DeleteRequested, DeleteSucceed, DeleteFailed } from 'src/books/@xstate/books.events';
import { BooksStates } from 'src/books/@xstate/books-states-enum';

export interface IBooksSchema {
    states: {
        idle: {},
        fetch_processing: {},
        create_processing: {},
        update_processing: {},
        delete_processing: {},
        done: {}
    }
}

export const booksConfig: MachineConfig<
    any,
    IBooksSchema,
    BooksEvents
> = {
    id: 'books_state_machine',
    initial: BooksStates.IDLE,
    states: {
        idle: {
            on: {
                [FetchRequested.type]: BooksStates.FETCH_PROCESSING,
                [CreateRequested.type]: BooksStates.CREATE_PROCESSING,
                [UpdateRequested.type]: BooksStates.UPDATE_PROCESSING,
                [DeleteRequested.type]: BooksStates.DELETE_PROCESSING
            }
        },
        fetch_processing: {
            invoke: {
                id: 'fetchBooks',
                src: 'fetchBooks'
            },
            on: {
                [FetchSucceed.type]: {
                    target: BooksStates.DONE,
                    actions: ['fetchSucceedProcess', 'showMessage']
                },
                [FetchFailed.type]: {
                    target: BooksStates.DONE,
                    actions: ['fetchFailedProcess', 'showMessage']
                }
            }
        },
        create_processing: {
            invoke: {
                id: 'createBook',
                src: 'createBook'
            },
            on: {
                [CreateSucceed.type]: {
                    target: BooksStates.DONE,
                    actions: ['createSucceedProcess', 'showMessage']
                },
                [CreateFailed.type]: {
                    target: BooksStates.DONE,
                    actions: ['createFailedProcess', 'showMessage']
                }
            }
        },
        update_processing: {
            invoke: {
                id: 'updateBook',
                src: 'updateBook'
            },
            on: {
                [UpdateSucceed.type]: {
                    target: BooksStates.DONE,
                    actions: ['updateSucceedProcess', 'showMessage']
                },
                [UpdateFailed.type]: {
                    target: BooksStates.DONE,
                    actions: ['updateFailedProcess', 'showMessage']
                }
            }
        },
        delete_processing: {
            invoke: {
                id: 'deleteBook',
                src: 'deleteBook'
            },
            on: {
                [DeleteSucceed.type]: {
                    target: BooksStates.DONE,
                    actions: ['deleteSucceedProcess', 'showMessage']
                },
                [DeleteFailed.type]: {
                    target: BooksStates.DONE,
                    actions: ['deleteFailedProcess', 'showMessage']
                }
            }
        },
        done: {
            type: 'final'
        }
    }
}
