import { Provider } from '@nestjs/common';
import { MongoDBEnums } from 'src/@databases/mongodb/mongodb-collections.enum';
import { Connection } from 'mongoose';
import { BookSchema } from 'src/books/schemas/book.schema';
import { BookRepositoryBase } from 'src/books/repositories/book-repository.base';
import { BookRepository } from 'src/books/repositories/book.repository';

export const BookCollectionProvider: Provider = {
    provide: MongoDBEnums.BOOK_COLLECTION,
    useFactory: (connection: Connection) => {
        return connection.model(
            MongoDBEnums.BOOK_COLLECTION,
            BookSchema
        );
    },
    inject: [MongoDBEnums.BOOK_CONNECTION_TOKEN]
}

export const BookRepositoryProvider: Provider = {
    provide: BookRepositoryBase,
    useClass: BookRepository,
    inject: [MongoDBEnums.BOOK_COLLECTION]
}
