import { Model, MongooseFilterQuery } from 'mongoose';
import { Book } from 'src/books/models/book.model';
import { CreateBookDto } from 'src/books/dtos/create-book.dto';
import { Injectable, InternalServerErrorException, Logger, Inject } from '@nestjs/common';
import { BookRepositoryBase } from 'src/books/repositories/book-repository.base';
import { UpdateBookDto } from 'src/books/dtos/update-book.dto';
import { MongoDBEnums } from 'src/@databases/mongodb/mongodb-collections.enum';

@Injectable()
export class BookRepository implements BookRepositoryBase {
    constructor(
        @Inject(MongoDBEnums.BOOK_COLLECTION)
        private bookRepository: Model<Book>
    ) { }

    async getBooks(conditions: MongooseFilterQuery<Book>[]): Promise<Book[]> {
        const defaultQuery = { $match: {} };

        return await this.bookRepository.aggregate([
            conditions || defaultQuery,
            {
                $addFields: {
                    id: '$_id'
                }
            },
            {
                $unset: ['_id', '__v']
            }
        ]);
    }

    async createBook(createBookDto: CreateBookDto): Promise<Book> {
        return this.bookRepository
            .create(createBookDto)
            .then(res => res)
            .catch(err => {
                new Logger().error(err);
                throw new InternalServerErrorException();
            })
    }

    async updateBook(id: string, updateBookDto: Partial<UpdateBookDto>): Promise<Book> {
        return this.bookRepository
            .findByIdAndUpdate(id, updateBookDto, { new: true })
            .then(res => res)
            .catch(err => {
                new Logger().error(err);
                throw new InternalServerErrorException();
            });
    }

    async deleteBook(id: string): Promise<void> {
        this.bookRepository
            .deleteOne({ _id: id })
            .catch(err => {
                new Logger().error(err);
                throw new InternalServerErrorException();
            });
    }
}
