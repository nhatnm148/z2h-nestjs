/* eslint-disable @typescript-eslint/no-unused-vars */
import { MongooseFilterQuery } from 'mongoose';
import { Book } from 'src/books/models/book.model';
import { CreateBookDto } from 'src/books/dtos/create-book.dto';
import { UpdateBookDto } from 'src/books/dtos/update-book.dto';

export abstract class BookRepositoryBase {
    getBooks(conditions: MongooseFilterQuery<Book>): Promise<Book[]> { return null; };

    createBook(createBookDto: CreateBookDto): Promise<Book> { return null; };

    updateBook(id: string, updateBookDto: Partial<UpdateBookDto>): Promise<Book> { return null; };

    deleteBook(id: string): Promise<void> { return null; };
}
