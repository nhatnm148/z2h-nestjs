import { Test } from '@nestjs/testing';
import { BookRepositoryProvider, BookCollectionProvider } from 'src/books/repositories/book.provider';
import { BooksService } from 'src/books/books.service';
import { SharedModule } from 'src/shared/shared.module';
import { CreateBookDto } from 'src/books/dtos/create-book.dto';
import { Book } from 'src/books/models/book.model';
import { DatabaseModule } from 'src/@databases/databse.module';
import { ConfigModule } from 'src/@config/config.module';
import { INestApplication } from '@nestjs/common';
import { AppModule } from 'src/@app/app.module';
import * as request from 'supertest';
import { UpdateBookDto } from 'src/books/dtos/update-book.dto';
import { BookRepositoryBase } from 'src/books/repositories/book-repository.base';
import { RespondBodyFilterService } from 'src/shared/services/respond-body-filter.service';

class BooksSpecHelper {
    /**
     * Prepare the payload to create new book
     * @param title Title of the book
     * @param author Name of the book's author
     * @returns CreateBookDto
     */
    static prepareCreateBookDto(title = '', author = ''): CreateBookDto {
        return { title, author };
    }

    /**
     * Prepare the payload to update existed book
     * @param title Title of the book
     * @param author Name of the book's author
     * @returns UpdateBookDto
     */
    static prepareUpdateBookDto(
        title: string = null, author: string = null
    ): Partial<UpdateBookDto> {
        if ((!title && !author) || (title && author)) {
            return { title, author };
        }

        if (!title) {
            return { author };
        }

        if (!author) {
            return { title };
        }
    }

    /**
     * Return a book payload existing in the DB (if no books exist, creates a new one then returns it)
     * @param booksRepo Book repository
     * @returns Returns the payload of the book to update
     */
    static async prepareBookToUpdate(booksRepo: BookRepositoryBase): Promise<Book> {
        return await booksRepo
            .createBook(BooksSpecHelper.prepareCreateBookDto('Be With You', 'Ichikawa Takuji'));
    }
}

describe('BOOKS', () => {
    jest.setTimeout(15000);

    let app: INestApplication;
    let booksService: BooksService;
    let booksRepo: BookRepositoryBase;
    let respondBodyFilterService: RespondBodyFilterService;

    beforeAll(async () => {
        const moduleRef = await Test.createTestingModule({
            imports: [
                SharedModule,
                DatabaseModule,
                ConfigModule,
                AppModule
            ],
            providers: [
                BooksService,
                BookCollectionProvider,
                BookRepositoryProvider
            ]
        }).compile();

        app = moduleRef.createNestApplication<INestApplication>();
        await app.init();
        booksService = await moduleRef.resolve<BooksService>(BooksService);
        booksRepo = moduleRef.get<BookRepositoryBase>(BookRepositoryBase);
        respondBodyFilterService = moduleRef.get<RespondBodyFilterService>(RespondBodyFilterService);
    });

    describe('CREATE BOOK', () => {
        let createBookDto: CreateBookDto;

        beforeEach(() => {
            createBookDto = BooksSpecHelper.prepareCreateBookDto();
        });

        it('Happy case', async () => {
            createBookDto = BooksSpecHelper
                .prepareCreateBookDto('Be With You', 'Ichikawa Takuji');

            const createdBook = await booksService
                .createBook(createBookDto)
                .toPromise();

            expect(createdBook).toEqual<Partial<Book>>({
                id: expect.any(String),
                ...createBookDto
            });
        });

        it('Empty title', async (done) => {
            createBookDto = BooksSpecHelper
                .prepareCreateBookDto('', 'Ichikawa Takuji');

            request(app.getHttpServer())
                .post('/books')
                .send(createBookDto)
                .expect(400)
                .end((err) => {
                    err ? done(err) : done();
                });
        });

        it('Empty author', async (done) => {
            createBookDto = BooksSpecHelper
                .prepareCreateBookDto('Be With You', '');

            request(app.getHttpServer())
                .post('/books')
                .send(createBookDto)
                .expect(400)
                .end((err) => {
                    err ? done(err) : done();
                });
        });

        it('No payload', async (done) => {
            request(app.getHttpServer())
                .post('/books')
                .expect(401)
                .end((err) => {
                    err ? done() : done();
                });
        });
    });

    describe('UPDATE BOOK', () => {
        let updateBookDto: Partial<UpdateBookDto>;

        beforeEach(() => {
            updateBookDto = BooksSpecHelper.prepareUpdateBookDto();
        });

        it('Happy case', async () => {
            updateBookDto = BooksSpecHelper
                .prepareUpdateBookDto('The Godfather', 'Mario Puzo');

            const { id } = await BooksSpecHelper.prepareBookToUpdate(booksRepo);

            const updatedBook = await booksService
                .updateBook(id, updateBookDto)
                .toPromise();

            expect(updatedBook).toEqual<Partial<Book>>({ id, ...updateBookDto });
        });

        it('Remains title', async () => {
            updateBookDto = BooksSpecHelper
                .prepareUpdateBookDto(null, 'Mario Puzo');

            const { id } = await BooksSpecHelper.prepareBookToUpdate(booksRepo);

            const updatedBook = await booksService
                .updateBook(id, updateBookDto)
                .toPromise();

            expect(updatedBook).toEqual<Partial<Book>>({
                id,
                title: updatedBook.title,
                ...updateBookDto
            });
        });

        it('Remains author', async () => {
            updateBookDto = BooksSpecHelper
                .prepareUpdateBookDto('The Godfather');

            const { id } = await BooksSpecHelper.prepareBookToUpdate(booksRepo);

            const updatedBook = await booksService
                .updateBook(id, updateBookDto)
                .toPromise();

            expect(updatedBook).toEqual<Partial<Book>>({
                id,
                author: updatedBook.author,
                ...updateBookDto
            });
        });

        it('Null title and author', async (done) => {
            updateBookDto = BooksSpecHelper
                .prepareUpdateBookDto();

            const bookToUpdate = await BooksSpecHelper
                .prepareBookToUpdate(booksRepo);

            request(app.getHttpServer())
                .put(`/books/${bookToUpdate.id}`)
                .send(updateBookDto)
                .expect(200)
                .end((err, res) => {
                    if (err) {
                        return done(err);
                    }

                    expect(res.body).toEqual<Partial<Book>>({
                        ...respondBodyFilterService.transform(bookToUpdate)
                    });

                    done();
                });
        });

        it('No payload', async () => {
            const bookToUpdate = await BooksSpecHelper
                .prepareBookToUpdate(booksRepo);

            const updatedBook = await booksService
                .updateBook(bookToUpdate.id, {})
                .toPromise();

            expect(updatedBook).toEqual<Partial<Book>>(
                respondBodyFilterService.transform(bookToUpdate)
            );
        });
    });
});
