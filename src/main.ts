import { NestFactory } from '@nestjs/core';
import 'dotenv/config';
import { AppModule } from './@app/app.module';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  app.enableCors();

  const options = new DocumentBuilder()
    .setTitle('Z2H APIs')
    .build();
  const document = SwaggerModule.createDocument(app, options);
  SwaggerModule.setup('apis', app, document);

  await app.listen(process.env.PORT || 3000);
}
bootstrap();
