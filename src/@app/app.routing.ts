import { Routes, RouterModule } from 'nest-router';
import { BooksModule } from 'src/books/books.module';
import { Module } from '@nestjs/common';
import { HeroesModule } from 'src/heroes/heroes.module';
import { DragonsModule } from 'src/dragons/dragons.module';
import { DragonSlayerProcessManagerModule } from 'src/dragon-slayer-process-manager/dragon-slayer-process-manager.module';
import { MessengerModule } from 'src/messenger/messenger.module';
import { MessengerUsersModule } from 'src/messenger-users/messenger-users.module';

export const routes: Routes = [
    {
        path: 'books',
        module: BooksModule
    },
    {
        path: 'heroes',
        module: HeroesModule
    },
    {
        path: 'dragons',
        module: DragonsModule
    },
    {
        path: 'dragon-slayer-process-manager',
        module: DragonSlayerProcessManagerModule
    },
    {
        path: 'messengers',
        module: MessengerModule
    },
    {
        path: 'messenger-users',
        module: MessengerUsersModule
    }
];

@Module({
    imports: [
        RouterModule.forRoutes(routes)
    ],
    exports: [
        RouterModule
    ]
})
export class AppRouting { }
