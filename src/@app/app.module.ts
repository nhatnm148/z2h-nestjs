import { Module } from '@nestjs/common';
import { BooksModule } from '../books/books.module';
import { AppRouting } from 'src/@app/app.routing';
import { SharedModule } from 'src/shared/shared.module';
import { DatabaseModule } from 'src/@databases/databse.module';
import { HeroesModule } from 'src/heroes/heroes.module';
import { DragonsModule } from 'src/dragons/dragons.module';
import { DragonSlayerProcessManagerModule } from 'src/dragon-slayer-process-manager/dragon-slayer-process-manager.module';
import { ConfigModule } from 'src/@config/config.module';
import { MsgBrokerModule } from 'src/@msg-brokers/msg-broker.module';
import { MessengerModule } from 'src/messenger/messenger.module';
import { MessengerUsersModule } from 'src/messenger-users/messenger-users.module';

@Module({
  imports: [
    BooksModule,
    HeroesModule,
    DragonsModule,
    DragonSlayerProcessManagerModule,
    MsgBrokerModule,
    ConfigModule,
    MessengerModule,
    MessengerUsersModule,
    SharedModule,
    AppRouting,
    DatabaseModule
  ],
  controllers: [],
  providers: [],
  exports: []
})
export class AppModule {}
