import { NestInterceptor, ExecutionContext, CallHandler } from '@nestjs/common';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

export class RespondBodyFilterInterceptor implements NestInterceptor {zz
    intercept(ctx: ExecutionContext, next: CallHandler): Observable<any> {
        return next
            .handle()
            .pipe(map(res => {
                if (!res || res.constructor === Array) {
                    return res;
                };

                const modifiedRes = res.toObject();

                if (modifiedRes._id) {
                    modifiedRes.id = modifiedRes._id;
                    delete modifiedRes._id;
                }

                if (modifiedRes.__v || !isNaN(modifiedRes.__v)) {
                    delete modifiedRes.__v;
                }

                return modifiedRes;
            }));
    }
}